package redis

import (
	"errors"

	log "bitbucket.org/to-increase/go-logger"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/elasticache"
)

func GetConnectionInfo(cluster string) (string, int64, error) {
	// log.SetLevel(log.DebugLevel)
	log.Debugf("GetConnectionInfo for cluster %v", cluster)

	client := elasticache.New(session.New())

	b := true
	res, err := client.DescribeCacheClusters(&elasticache.DescribeCacheClustersInput{
		CacheClusterId:    &cluster,
		ShowCacheNodeInfo: &b,
	})
	if err != nil {
		log.Errorf("Could not retrieve redis cluster %v. Error: %v", cluster, err)
		return "", -1, err
	}
	log.Debugf("Response: %v", res)

	if len(res.CacheClusters) > 0 {
		c := res.CacheClusters[0]
		if *c.CacheClusterStatus == "available" &&
			len(c.CacheNodes) > 0 {
			n := c.CacheNodes[0]
			return *n.Endpoint.Address, *n.Endpoint.Port, nil
		}
	}
	log.Errorf("Could not retrieve connection info for redis cluster %v. Cluster may be inactive or may not have any nodes", cluster)
	return "", -1, errors.New("No connection info")
}
