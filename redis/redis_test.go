package redis

import (
	"flag"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

var cluster string

func init() {
	flag.StringVar(&cluster, "cluster", "", "the cluster")
}

func TestMain(m *testing.M) {
	flag.Parse()
	os.Exit(m.Run())
}

// TODO: FIX THIS RLCOMTE
func TestGetConnectionInfo(t *testing.T) {
	return
	if cluster == "" {
		cluster = "xenon-monitoring"
	}
	s, p, err := GetConnectionInfo(cluster)

	assert.Nil(t, err)
	assert.NotEmpty(t, s)
	assert.Equal(t, int64(6379), p)
}
