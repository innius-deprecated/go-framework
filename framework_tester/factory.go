package framework_tester

import (
	"net/http"
	"strings"

	"bitbucket.org/to-increase/go-config"
	"bitbucket.org/to-increase/go-context"
	fwconfig "bitbucket.org/to-increase/go-framework/config"
	"bitbucket.org/to-increase/go-framework/dependencies"
	"bitbucket.org/to-increase/go-framework/types"
	"github.com/labstack/echo"
	"net/http/httptest"
)

// Create a new Service Context object.
func NewServiceContext() TestSvcContext {
	return NewTestSvcContext(NewServiceContextFromEcho(nil))
}

// Create a new Service Context object from an echo context
func NewServiceContextFromEcho(e echo.Context) TestSvcContext {
	c := context.NewCtxFromEcho(e)
	c.Logger().SetLevel(default_log_level)
	return NewTestSvcContext(c)
}

// Create a new Task Context object
func NewTaskContext() context.TaskContext {
	c := context.NewTaskCtx()
	c.Logger().SetLevel(default_log_level)
	return c
}

// Create a new and empty dependency container
func NewDependenciesContainer() types.MutableContainer {
	c := dependencies.NewContainer()
	c.SetConfiguration(NewEmptyConfigurator())
	return c
}

// Create a new configurator so you can read DynamicValues with default properties
func NewEmptyConfigurator() types.Configuration {
	return fwconfig.NewFrameworkConfig("", config.NewConfiguratorWithLogger(NewTaskContext().Logger()))
}

// Conveniently create the paramaters needed by a handler func
func NewHandlerFuncParams() (TestSvcContext, types.MutableContainer) {
	a, b, _ := NewHandlerFuncParamsWithBody("")
	return a, b
}

// Conveniently create the parameters needed by a handler func
func NewHandlerFuncParamsWithBody(body string) (TestSvcContext, types.MutableContainer, *httptest.ResponseRecorder) {
	// In the handler itself we don't care about URL + Method type.
	req := httptest.NewRequest("POST", "/", strings.NewReader(body))
	req.Header.Add(http.CanonicalHeaderKey("content-type"), "application/json")
	return NewHandlerFuncParamsWithRequest(req)
}

// Conveniently create the parameters needed by a handler func
func NewHandlerFuncParamsWithRequest(req *http.Request) (TestSvcContext, types.MutableContainer, *httptest.ResponseRecorder) {
	e := echo.New()
	rec := httptest.NewRecorder()
	ectx := e.NewContext(req, rec)
	return NewTestSvcContext(NewServiceContextFromEcho(ectx)), NewDependenciesContainer(), rec
}
func NewHandlerFunc(url string) (TestSvcContext, types.MutableContainer, *httptest.ResponseRecorder) {
	req := httptest.NewRequest("GET", url, nil)
	return NewHandlerFuncParamsWithRequest(req)
}
