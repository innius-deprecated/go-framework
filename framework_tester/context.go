package framework_tester

import (
	"net/http"
	"time"

	gocontext "context"

	"bitbucket.org/to-increase/go-auth/claims"
	"bitbucket.org/to-increase/go-context"
	log "bitbucket.org/to-increase/go-logger"
	"github.com/labstack/echo"
)

type TestSvcContext interface {
	context.SvcContext
	SetParam(name string, value string)
	SetQuery(name string, value string)
	// Return a NEW context object with the given claims.
	WithClaims(cl *claims.Claims) TestSvcContext
}

type testctx struct {
	ctx       context.SvcContext
	params    []tuple
	query     []tuple
	claims    *claims.Claims
	gocontext gocontext.Context
}

type tuple struct {
	Name  string
	Value string
}

func NewTestSvcContext(c context.SvcContext) TestSvcContext {
	return &testctx{
		ctx:       c,
		params:    make([]tuple, 0),
		query:     make([]tuple, 0),
		gocontext: gocontext.Background(),
	}
}

// overridden behaviour

func (t *testctx) SetParam(name string, value string) {
	t.params = append(t.params, tuple{
		Name:  name,
		Value: value,
	})
}

func (t *testctx) Param(name string) string {
	for _, v := range t.params {
		if v.Name == name {
			return v.Value
		}
	}
	if t.ctx != nil {
		return t.ctx.Param(name)
	}
	return ""
}

func (t *testctx) SetQuery(name string, value string) {
	t.query = append(t.query, tuple{
		Name:  name,
		Value: value,
	})
}

func (t *testctx) Query(name string) string {
	for _, v := range t.query {
		if v.Name == name {
			return v.Value
		}
	}
	return ""
}

// default behaviour

func (t *testctx) EchoContext() echo.Context {
	return t.ctx.EchoContext()
}

func (t *testctx) Logger() *log.Logger {
	return t.ctx.Logger()
}

func (t *testctx) HasEchoContext() bool {
	return t.ctx.HasEchoContext()
}

func (t *testctx) GetRequestIDHeader() (string, string) {
	return t.ctx.GetRequestIDHeader()
}

func (t *testctx) GetToken() string {
	return t.ctx.GetToken()
}

func (t *testctx) GetLogLevelHeader() (string, string) {
	return t.ctx.GetLogLevelHeader()
}

func (t *testctx) DeriveTaskContext() context.TaskContext {
	return t.ctx.DeriveTaskContext()
}

func (t *testctx) Claims() *claims.Claims {
	if t.claims != nil {
		return t.claims
	}
	return t.ctx.Claims()
}

func (t *testctx) Request() *http.Request {
	return t.ctx.Request()
}

func (t *testctx) Path() string {
	return t.ctx.Path()
}

func (t *testctx) Response() *echo.Response {
	return t.ctx.Response()
}

func (t *testctx) Form(name string) string {
	return t.ctx.Form(name)
}

func (t *testctx) Get(key string) interface{} {
	return t.ctx.Get(key)
}

func (t *testctx) Set(key string, val interface{}) {
	t.ctx.Set(key, val)
}

func (t *testctx) Bind(i interface{}) error {
	return t.ctx.Bind(i)
}

func (t *testctx) Render(code int, name string, data interface{}) error {
	return t.ctx.Render(code, name, data)
}

func (t *testctx) HTML(code int, html string) error {
	return t.ctx.HTML(code, html)
}

func (t *testctx) String(code int, s string) error {
	return t.ctx.String(code, s)
}

func (t *testctx) JSON(code int, i interface{}) error {
	return t.ctx.JSON(code, i)
}

func (t *testctx) JSONBlob(code int, b []byte) error {
	return t.ctx.JSONBlob(code, b)
}

func (t *testctx) JSONP(code int, callback string, i interface{}) error {
	return t.ctx.JSONP(code, callback, i)
}

func (t *testctx) XML(code int, i interface{}) error {
	return t.ctx.XML(code, i)
}

func (t *testctx) Attachment(file string, name string) error {
	return t.ctx.Attachment(file, name)
}

func (t *testctx) NoContent(code int) error {
	return t.ctx.NoContent(code)
}

func (t *testctx) Redirect(code int, url string) error {
	return t.ctx.Redirect(code, url)
}

func (t *testctx) Error(err error) {
	t.ctx.Error(err)
}

func (t *testctx) Echo() *echo.Echo {
	return t.ctx.Echo()
}

func (t *testctx) WithToken(token string) context.SvcContext {
	return t.ctx.WithToken(token)
}

func (t *testctx) WithClaims(cl *claims.Claims) TestSvcContext {
	return &testctx{
		ctx:       t.ctx,
		params:    t.params,
		query:     t.query,
		claims:    cl,
		gocontext: t.gocontext,
	}
}

// standard go context interface
func (c *testctx) Deadline() (deadline time.Time, ok bool) {
	return c.gocontext.Deadline()
}

func (c *testctx) Done() <-chan struct{} {
	return c.gocontext.Done()
}

func (c *testctx) Err() error {
	return c.gocontext.Err()
}

func (c *testctx) Value(key interface{}) interface{} {
	return c.gocontext.Value(key)
}
