package framework_tester

import (
	"time"

	"bitbucket.org/to-increase/go-auth/claims"
	"bitbucket.org/to-increase/go-auth/pep"
	"bitbucket.org/to-increase/go-auth/pep/actions"
	"bitbucket.org/to-increase/go-config"
	"bitbucket.org/to-increase/go-config/sources"
	"bitbucket.org/to-increase/go-config/types"
	"bitbucket.org/to-increase/go-context"
	"bitbucket.org/to-increase/go-framework"
	frameworkconfig "bitbucket.org/to-increase/go-framework/config"
	"bitbucket.org/to-increase/go-framework/constants"
	"bitbucket.org/to-increase/go-framework/dependencies"
	"bitbucket.org/to-increase/go-framework/router"
	"bitbucket.org/to-increase/go-framework/smoke"
	frameworktypes "bitbucket.org/to-increase/go-framework/types"
	"bitbucket.org/to-increase/go-trn"
	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
)

const default_log_level string = "panic"
const default_log_key string = "loglevel"

type TestServerContainer interface {
	Reload(func() error)
	Version(func() string)
	BuildTime(func() string)
	Name(func() string)
	ApplyConfig(func(context.TaskContext, *echo.Echo) error)
	Configurators(func(context.TaskContext) ([]sources.ConfigurationSource, error))
	RegisterDependencies(func(context.TaskContext, frameworktypes.MutableContainer) error)
	StaticHandlers(func(context.TaskContext) map[string]string)
	PublicHandlers(func(context.TaskContext, frameworktypes.Router))
	ProtectedHandlers(func(context.TaskContext, frameworktypes.SecuredRouter))
	Smokers(func(context.TaskContext, frameworktypes.Container) (*smoke.Smoker, []*smoke.Smoker))
	BeforeServe(func(context.TaskContext, frameworktypes.Container) error)
	WithLogLevel(string)
	WithPrincipal(PrincipalFunc)
	WithAuthorizationHandler(pep.AuthorizationHandlerFunc)
	Configure()
	Echo() *echo.Echo
}

type PrincipalFunc func() (string, string)

type testserver struct {
	service                    framework.Service
	loglevel                   string
	echo                       *echo.Echo
	actualReload               func() error
	actualVersion              func() string
	actualBuildTime            func() string
	actualName                 func() string
	actualApplyConfig          func(context.TaskContext, *echo.Echo) error
	actualConfigurators        func(context.TaskContext) ([]sources.ConfigurationSource, error)
	actualRegisterDependencies func(context.TaskContext, frameworktypes.MutableContainer) error
	actualStaticHandlers       func(context.TaskContext) map[string]string
	actualPublicHandlers       func(context.TaskContext, frameworktypes.Router)
	actualProtectedHandlers    func(context.TaskContext, frameworktypes.SecuredRouter)
	actualSmokers              func(context.TaskContext, frameworktypes.Container) (*smoke.Smoker, []*smoke.Smoker)
	actualBeforeServe          func(context.TaskContext, frameworktypes.Container) error
	principalFunc              PrincipalFunc
	authorizationHandler       pep.AuthorizationHandlerFunc
}

func defaultPrincipalFunc() (string, string) {
	return "framework-default-company-id", "framework-default-user-id"
}

func defaultAuthorizationHandler(c context.SvcContext, t *trn.TRN, a actions.Action) error {
	// Everything is authorized by default.
	return nil
}

func TestServer(s framework.Service) TestServerContainer {
	return &testserver{
		service:                    s,
		loglevel:                   default_log_level,
		echo:                       echo.New(),
		actualVersion:              s.Version,
		actualBuildTime:            s.BuildTime,
		actualName:                 s.Name,
		actualApplyConfig:          s.ApplyConfig,
		actualConfigurators:        s.Configurators,
		actualRegisterDependencies: s.RegisterDependencies,
		actualStaticHandlers:       s.StaticHandlers,
		actualPublicHandlers:       s.PublicHandlers,
		actualProtectedHandlers:    s.ProtectedHandlers,
		actualSmokers:              s.Smokers,
		principalFunc:              defaultPrincipalFunc,
		authorizationHandler:       defaultAuthorizationHandler,
	}
}

func (ts *testserver) Reload(f func() error) {
	ts.actualReload = f
}

func (ts *testserver) Version(f func() string) {
	ts.actualVersion = f
}

func (ts *testserver) BuildTime(f func() string) {
	ts.actualBuildTime = f
}

func (ts *testserver) Name(f func() string) {
	ts.actualName = f
}

func (ts *testserver) ApplyConfig(f func(c context.TaskContext, e *echo.Echo) error) {
	ts.actualApplyConfig = f
}

func (ts *testserver) Configurators(f func(context.TaskContext) ([]sources.ConfigurationSource, error)) {
	ts.actualConfigurators = f
}

func (ts *testserver) RegisterDependencies(f func(c context.TaskContext, di frameworktypes.MutableContainer) error) {
	ts.actualRegisterDependencies = f
}

func (ts *testserver) StaticHandlers(f func(context.TaskContext) map[string]string) {
	ts.actualStaticHandlers = f
}

func (ts *testserver) PublicHandlers(f func(context.TaskContext, frameworktypes.Router)) {
	ts.actualPublicHandlers = f
}

func (ts *testserver) ProtectedHandlers(f func(context.TaskContext, frameworktypes.SecuredRouter)) {
	ts.actualProtectedHandlers = f
}

func (ts *testserver) Smokers(f func(context.TaskContext, frameworktypes.Container) (*smoke.Smoker, []*smoke.Smoker)) {
	ts.actualSmokers = f
}

func (ts *testserver) BeforeServe(f func(context.TaskContext, frameworktypes.Container) error) {
	ts.actualBeforeServe = f
}

func (ts *testserver) WithLogLevel(loglevel string) {
	ts.loglevel = loglevel
}

func (ts *testserver) WithPrincipal(f PrincipalFunc) {
	ts.principalFunc = f
}

func (ts *testserver) WithAuthorizationHandler(f pep.AuthorizationHandlerFunc) {
	ts.authorizationHandler = f
}

func (ts *testserver) Configure() {
	ctx := context.NewTaskCtx()
	level, _ := logrus.ParseLevel(ts.loglevel)
	logrus.SetLevel(level)

	di := dependencies.NewContainer()

	middleware := []frameworktypes.MiddlewareFunc{muter(ts.loglevel), ts.authenticationHandler()}

	l := ctx.Logger()

	l.Infof("Bootstrapping %v", ts.actualName())
	cs, err := ts.actualConfigurators(ctx)
	if err != nil {
		l.Errorf("Configurators could not be initialized: %v\n", err)
	}
	cf := config.NewConfiguratorWithLogger(ctx.Logger(), cs...)

	// Retrieve the loglevel from a config source and update the ctx logger when it changes
	dll := cf.GetString(ts.actualName()+"/"+default_log_key, default_log_level)
	l.SetLevel(dll.Value())
	dll.OnChange(func(path types.Path, value string) {
		l.SetLevel(value)
	})

	// Give the configurators some time to settle down.
	time.Sleep(50 * time.Millisecond)

	err = ts.actualApplyConfig(ctx, ts.Echo())
	if err != nil {
		l.Errorf("Failed to apply condiguration. Reason: %v", err)
		return
	}
	l.Debugf("Configuration applied.")

	di.SetConfiguration(frameworkconfig.NewFrameworkConfig(ts.actualName(), cf))

	err = ts.actualRegisterDependencies(ctx, di)
	if err != nil {
		l.Errorf("Failed to register dependencies. Reason: %v", err)
		return
	}

	// NOTE store the authorization handler in the DI container, to support the custom authorization scenario
	di.Set(constants.DI_FW_AUTH_HANDLER, ts.authorizationHandler)

	idi := di.Immutable()
	l.Debugf("Registering dependencies finished.")

	ts.register_static_routes(ctx)
	l.Debugf("Static routes applied.")

	ts.register_public_handlers(ctx, idi, middleware)
	l.Debugf("Public routes applied.")

	ts.register_protected_handlers(ctx, idi, middleware, ts.authorizationHandler)
	l.Debugf("Private routes applied.")

	// Disable health endpoint
	//health.Health(ts.Echo(), ts.actualName(), ts.actualVersion(), ts.actualBuildTime())

	// Disable smoke endpoint
	//self, children := ts.actualSmokers(ctx, idi)
	//smoke.SmokeTest(ts.Echo(), ts.actualName(), ts.actualVersion(), ts.actualBuildTime(), self, children)
}

func (ts *testserver) Echo() *echo.Echo {
	return ts.echo
}

func (ts *testserver) register_static_routes(ctx context.TaskContext) {
	for route, path := range ts.actualStaticHandlers(ctx) {
		ctx.Logger().Debugf("Registering static path %v with dir %v", route, path)
		// TODO enable static routes in framework
		// ts.echo.Static(route, path)
	}
}

func (ts *testserver) register_public_handlers(ctx context.TaskContext, di frameworktypes.Container, mw []frameworktypes.MiddlewareFunc) {
	r := router.NewRouter()
	ts.actualPublicHandlers(ctx, r)

	r.Apply(ctx.Logger(), ts.echo, ts.actualName(), di)
}

func (ts *testserver) register_protected_handlers(ctx context.TaskContext, di frameworktypes.Container, gmw []frameworktypes.MiddlewareFunc, pepf pep.AuthorizationHandlerFunc) {
	r := router.NewSecuredRouter(gmw, pepf)
	ts.actualProtectedHandlers(ctx, r)

	r.Apply(ctx.Logger(), ts.echo, ts.actualName(), di)
}

func muter(level string) func(context.SvcContext) error {
	return func(c context.SvcContext) error {
		c.Logger().SetLevel(level)
		return nil
	}
}

func (ts *testserver) authenticationHandler() func(context.SvcContext) error {
	return func(c context.SvcContext) error {
		company, userid := ts.principalFunc()

		clms := &claims.Claims{}
		clms.Principal.CompanyId = company
		clms.Principal.UserId = userid

		claims.ToEchoContext(c.EchoContext(), clms)

		return nil
	}
}
