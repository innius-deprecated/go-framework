package framework_tester

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestConfigurableParams(t *testing.T) {
	_ctx := NewServiceContext()
	ctx := NewTestSvcContext(_ctx)
	ctx.SetParam("foo", "bar")
	assert.Equal(t, "bar", ctx.Param("foo"))
	assert.Equal(t, "", ctx.Param("abc"))
}
func TestConfigurableQuery(t *testing.T) {
	_ctx := NewServiceContext()
	ctx := NewTestSvcContext(_ctx)
	ctx.SetQuery("foo", "bar")
	assert.Equal(t, "bar", ctx.Query("foo"))
	assert.Equal(t, "", ctx.Param("abc"))
}
