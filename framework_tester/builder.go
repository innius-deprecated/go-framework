package framework_tester

import (
	"net/http"
	"strings"

	"net/http/httptest"

	"bitbucket.org/to-increase/go-auth/claims"
	"bitbucket.org/to-increase/go-config"
	"bitbucket.org/to-increase/go-config/sources"
	"bitbucket.org/to-increase/go-context"
	fwconfig "bitbucket.org/to-increase/go-framework/config"
	"bitbucket.org/to-increase/go-framework/dependencies"
	"bitbucket.org/to-increase/go-framework/types"
	"github.com/labstack/echo"
)

type FrameworkTester interface {
	// Invoke the preconfigured handler
	Invoke() error
	// The recorded status code
	Status() int
	// The recorder response body.
	Response() string
	Context() TestSvcContext
	Container() types.Container
	MutableContainer() types.MutableContainer
}

type theFrameworkTester struct {
	ctx              context.SvcContext
	serviceContext   context.SvcContext
	testSvcContext   TestSvcContext
	taskContext      context.TaskContext
	mutableContainer types.MutableContainer
	container        types.Container
	responseRecorder *httptest.ResponseRecorder
	handler          types.Handler
}

func (t *theFrameworkTester) Invoke() error {
	return t.handler(t.ctx, t.container)
}

func (t *theFrameworkTester) Status() int {
	return t.responseRecorder.Code
}

func (t *theFrameworkTester) Response() string {
	return t.responseRecorder.Body.String()
}

func (t *theFrameworkTester) Context() TestSvcContext {
	return t.testSvcContext
}

func (t *theFrameworkTester) Container() types.Container {
	return t.container
}

func (t *theFrameworkTester) MutableContainer() types.MutableContainer {
	return t.mutableContainer
}

type FrameworkTestBuilder struct {
	req       *http.Request
	dynprops  *sources.MemoryConfigurationSource
	container types.MutableContainer
	handler   types.Handler
	claims    *claims.Claims
	params    []tuple
}

func NewTest() *FrameworkTestBuilder {
	c := dependencies.NewContainer()
	b := sources.NewMemorySource()
	configurator := config.NewConfiguratorWithLogger(NewTaskContext().Logger(), b)

	c.SetConfiguration(fwconfig.NewFrameworkConfig("", configurator))

	return &FrameworkTestBuilder{
		req:       httptest.NewRequest(http.MethodGet, "/", nil),
		dynprops:  b,
		container: c,
		handler:   nil,
		claims:    &claims.Claims{},
		params:    make([]tuple, 0),
	}
}

func (b *FrameworkTestBuilder) WithDynamicProperty(key string, value interface{}) *FrameworkTestBuilder {
	b.dynprops.Set(key, value)
	return b
}

func (b *FrameworkTestBuilder) WithDependency(key string, value interface{}) *FrameworkTestBuilder {
	b.container.Set(key, value)
	return b
}

func (b *FrameworkTestBuilder) WithHandler(h types.Handler) *FrameworkTestBuilder {
	b.handler = h
	return b
}

func (b *FrameworkTestBuilder) WithGet(url string) *FrameworkTestBuilder {
	b.req = httptest.NewRequest(http.MethodGet, url, nil)
	return b
}

func (b *FrameworkTestBuilder) WithPost(url string, body string) *FrameworkTestBuilder {
	b.req = httptest.NewRequest(http.MethodPost, url, strings.NewReader(body))
	b.req.Header.Add(http.CanonicalHeaderKey("content-type"), "application/json")
	return b
}

func (b *FrameworkTestBuilder) WithDelete(url string) *FrameworkTestBuilder {
	b.req = httptest.NewRequest(http.MethodDelete, url, nil)
	return b
}

func (b *FrameworkTestBuilder) WithPatch(url string, body string) *FrameworkTestBuilder {
	b.req = httptest.NewRequest(http.MethodPatch, url, strings.NewReader(body))
	return b
}

func (b *FrameworkTestBuilder) WithPut(url string, body string) *FrameworkTestBuilder {
	b.req = httptest.NewRequest(http.MethodPut, url, strings.NewReader(body))
	return b
}

func (b *FrameworkTestBuilder) WithHeader(name string, value string) *FrameworkTestBuilder {
	b.req.Header.Set(name, value)
	return b
}

func (b *FrameworkTestBuilder) WithClaims(c *claims.Claims) *FrameworkTestBuilder {
	b.claims = c
	return b
}
func (b *FrameworkTestBuilder) WithPrincipal(company, person string) *FrameworkTestBuilder {
	b.claims.Principal.CompanyId = company
	b.claims.Principal.UserId = person
	return b
}

func (b *FrameworkTestBuilder) WithParam(name, value string) *FrameworkTestBuilder {
	b.params = append(b.params, tuple{
		Name:  name,
		Value: value,
	})
	return b
}

func (b *FrameworkTestBuilder) Build() FrameworkTester {
	e := echo.New()
	rec := httptest.NewRecorder()
	ectx := e.NewContext(b.req, rec)

	ctx := NewServiceContextFromEcho(ectx).WithClaims(b.claims)

	for _, p := range b.params {
		ctx.SetParam(p.Name, p.Value)
	}

	return &theFrameworkTester{
		ctx:              ctx,
		serviceContext:   ctx,
		testSvcContext:   ctx,
		taskContext:      ctx.DeriveTaskContext(),
		mutableContainer: b.container,
		container:        b.container.Immutable(),
		responseRecorder: rec,
		handler:          b.handler,
	}
}
