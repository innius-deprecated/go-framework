package util

import (
	"errors"
	"fmt"
	"net"
	"net/url"
	"runtime"

	"bitbucket.org/to-increase/go-feature-flag"
	"bitbucket.org/to-increase/go-feature-flag/features"
	"github.com/benschw/dns-clb-go/clb"
	"github.com/benschw/dns-clb-go/dns"
)

type ServiceURLHandler func(string) (*url.URL, error)

// GetServiceURL returns the endpoint for a specified service name based on the consul / dns configuration
func GetServiceURL(serviceName string) (*url.URL, error) {
	address, err := getServiceAddress(serviceName)
	if err != nil {
		return nil, err
	}
	return &url.URL{
		Scheme: "http",
		Host:   address.String(),
	}, nil
}

func getServiceAddress(name string) (*dns.Address, error) {
	if runtime.GOOS == "windows" {
		return nil, fmt.Errorf("could not resolve address of %v on windows", name)
	}

	if featureflags.FeatureEnabled(features.Route53ServiceDiscovery, name) {
		if addr, err := lookupSRV(name); err != nil {
			return nil, err
		} else if addr != nil {
			return &dns.Address{Address: addr.Target, Port: addr.Port}, nil
		}
	}

	srvName := fmt.Sprintf("%v.service.consul", name)
	c := clb.New()

	if addr, err := c.GetAddress(srvName); err != nil {
		return nil, err
	} else {
		return &addr, nil
	}
}

func lookupSRV(serviceName string) (*net.SRV, error) {
	var addrs []*net.SRV
	var err error
	if _, addrs, err = net.LookupSRV(serviceName, "tcp", fmt.Sprintf("%s.servicediscovery.internal", featureflags.GetEnvironmentName())); err != nil {
		return nil, err
	}
	for _, addr := range addrs {
		return addr, nil
	}
	return nil, errors.New("No record found")
}

func GetServiceAddress(name string) (string, error) {
	address, err := getServiceAddress(name)
	if err != nil {
		return "", err
	}
	return address.String(), nil

}
