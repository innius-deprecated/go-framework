package util

import "fmt"

// Get the (external) base name of a given microservice.
func GetExternalBaseServiceUri(stackname string, servicename string) string {
	return fmt.Sprintf("%v/api/%v", GetExternalBaseUri(stackname), servicename)
}

// Get the (external) base name of a whole stack.
func GetExternalBaseUri(stackname string) string {
	return fmt.Sprintf("https://%v.innius.com", GetExternalName(stackname))
}

// Get the (internal) base uri of a given microservice, including its port.
func GetInternalServiceURIAndPort(servicename string) string {
	return GetInternalServiceURI(servicename) + ":8080"
}

// Get the (internal) base uri of a given microservice
func GetInternalServiceURI(servicename string) string {
	return fmt.Sprintf("http://%v.service.consul", servicename)
}

// Return the external name of the given stack.
// This will return a 'symbolic' name, such as 'live', 'dev', etc.
func GetExternalName(stackname string) string {
	switch stackname {
	case "xenon":
		return "dev"
	case "chair":
		return "test"
	case "oak":
		return "demo"
	case "steam":
		return "live"
	default:
		return stackname
	}
}
