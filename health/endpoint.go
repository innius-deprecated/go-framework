package health

import (
	"net/http"
	"time"

	"bitbucket.org/to-increase/go-context"
	"bitbucket.org/to-increase/go-framework/types"
	"bitbucket.org/to-increase/go-framework/util"
)

type ServiceHealth struct {
	ServiceName string `json:"servicename"`
	Version     string `json:"version"`
	BuildTime   string `json:"buildtime"`
	State       string `json:"state"`
	HostName    string `json:"hostname"`
	TimeStamp   int64  `json:"timestamp"`
}

func HealthHandler(name string, version string, buildtime string) types.Handler {
	return func(c context.SvcContext, di types.Container) error {
		s := ServiceHealth{
			ServiceName: name,
			Version:     version,
			BuildTime:   buildtime,
			State:       "ok",
			HostName:    util.GetHostname(),
			TimeStamp:   time.Now().Unix(),
		}

		return c.JSON(http.StatusOK, s)
	}
}
