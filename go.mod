module bitbucket.org/to-increase/go-framework

require (
	bitbucket.org/to-increase/go-auth v1.0.0
	bitbucket.org/to-increase/go-cache v1.0.0
	bitbucket.org/to-increase/go-config v0.0.0-20171013123001-3640759488ef
	bitbucket.org/to-increase/go-context v1.0.0
	bitbucket.org/to-increase/go-echo-xray v1.0.0
	bitbucket.org/to-increase/go-feature-flag v1.0.1
	bitbucket.org/to-increase/go-logger v1.0.0
	bitbucket.org/to-increase/go-mountebank v1.0.0
	bitbucket.org/to-increase/go-sdk v1.0.6
	bitbucket.org/to-increase/go-trn v1.0.1
	github.com/afex/hystrix-go v0.0.0-20180502004556-fa1af6a1f4f5
	github.com/aws/aws-sdk-go v1.16.0
	github.com/aws/aws-xray-sdk-go v1.0.0-rc.8
	github.com/benschw/dns-clb-go v0.1.2-0.20151027182544-d27e15d9d464
	github.com/didip/tollbooth v4.0.0+incompatible
	github.com/labstack/echo v3.3.5+incompatible
	github.com/miekg/dns v1.1.1 // indirect
	github.com/pkg/errors v0.8.0
	github.com/sirupsen/logrus v1.2.0
	github.com/stretchr/testify v1.2.2
	github.com/valyala/fasthttp v1.0.0
	gopkg.in/tylerb/graceful.v1 v1.2.15
)
