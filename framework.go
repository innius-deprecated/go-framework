package framework

import (
	"net"
	"net/http"

	"github.com/aws/aws-xray-sdk-go/strategy/ctxmissing"

	"bitbucket.org/to-increase/go-auth"
	"bitbucket.org/to-increase/go-cache"
	"bitbucket.org/to-increase/go-context"
	frameworkconfig "bitbucket.org/to-increase/go-framework/config"
	"bitbucket.org/to-increase/go-framework/health"
	frameworktypes "bitbucket.org/to-increase/go-framework/types"
	log "bitbucket.org/to-increase/go-logger"
	"bitbucket.org/to-increase/go-sdk/services/authentication"
	"bitbucket.org/to-increase/go-sdk/services/authorization"

	"bytes"
	"fmt"
	_ "net/http/pprof"
	"os"
	"os/signal"
	"strings"
	"time"

	"bitbucket.org/to-increase/go-auth/pep"
	"bitbucket.org/to-increase/go-config"
	"bitbucket.org/to-increase/go-config/sources"
	configtypes "bitbucket.org/to-increase/go-config/types"
	"bitbucket.org/to-increase/go-framework/admin"
	"bitbucket.org/to-increase/go-framework/constants"
	"bitbucket.org/to-increase/go-framework/dependencies"
	"bitbucket.org/to-increase/go-framework/router"
	"bitbucket.org/to-increase/go-framework/smoke"
	"github.com/afex/hystrix-go/hystrix"
	// Importing the plugins enables collection of AWS resource information at runtime
	// _ "github.com/aws/aws-xray-sdk-go/plugins/ec2"
	"bitbucket.org/to-increase/go-echo-xray"
	client2 "bitbucket.org/to-increase/go-sdk/client"
	_ "github.com/aws/aws-xray-sdk-go/plugins/ecs"
	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/pkg/errors"
	"gopkg.in/tylerb/graceful.v1"
)

var Name string
var Version string
var Build_time string

type Service interface {
	// Reload the (micro)service
	Reload() error
	// The build version (git hash?) of the artifact
	Version() string
	// A string indicating the build time.
	BuildTime() string
	// The service name
	Name() string
	// Register dependencies
	RegisterDependencies(context.TaskContext, frameworktypes.MutableContainer) error
	// Implement any service configuration here
	ApplyConfig(context.TaskContext, *echo.Echo) error
	// Register configurators
	Configurators(c context.TaskContext) ([]sources.ConfigurationSource, error)
	// Register static handlers
	StaticHandlers(context.TaskContext) map[string]string
	// Register handlers for public routes
	PublicHandlers(context.TaskContext, frameworktypes.Router)
	// Register handlers for protected routes
	ProtectedHandlers(context.TaskContext, frameworktypes.SecuredRouter)
	// Register handlers for private routes (e.g. from within VPC)
	PrivateHandlers(context.TaskContext, frameworktypes.Router)
	// An optional hook that will be executed just before serving the microservice
	BeforeServe(context.TaskContext, frameworktypes.Container) error
	// Optional signal handlers
	SignalHandlers(context.TaskContext) []frameworktypes.SignalHandler
	// Smoketest info
	Smokers(context.TaskContext, frameworktypes.Container) (*smoke.Smoker, []*smoke.Smoker)
}

type DefaultService struct {
	name       string
	version    string
	build_time string
}

// TODO: Make a deep clone? It now basically is a singleton (which currently is the whole concept -- a microservice)
var DefaultServiceInstance = &DefaultService{
	name:       Name,
	version:    Version,
	build_time: Build_time,
}

// authenticator middleware which verifies the token
func authenticationHandler(cfg config.Configuration, endpoint configtypes.DynamicStringValue) frameworktypes.MiddlewareFunc {
	client := authentication.New(client2.Endpoint(endpoint.Value()))

	return func(c context.SvcContext) error {
		return auth.TokenHandler(c, client)
	}
}

const minute int = 60

func authorizationHandler(cfg frameworktypes.Configuration) pep.AuthorizationHandlerFunc {
	endpoint := cfg.GetPrefixedString(constants.DEFAULT_AUTHORIZATION_ENDPOINT, "")
	client := authorization.New(client2.Endpoint(endpoint.Value()))

	// configure the cache
	ttl := cfg.GetInt(constants.AUTHORIZATION_CACHE_TTL, 10*minute)
	cluster := cfg.GetPrefixedString(constants.AUTHORIZATION_CACHE_CLUSTER, "")
	var cc cache.Cache
	if cluster.Value() != "" {
		cc = cache.NewCacheClient(cluster.Value(), ttl.Value())
	}

	return pep.AuthorizationHandler(client, cc)
}

func get_ip() (string, error) {
	client := &http.Client{
		Timeout: 1 * time.Second,
	}
	resp, err := client.Get("http://169.254.169.254/latest/meta-data/local-ipv4")
	if err != nil {
		return "unknown", errors.Wrap(err, "Cannot retrieve ip")
	}
	defer resp.Body.Close()
	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)
	return buf.String(), nil
}

func metrics_prefix(s Service, cfg frameworktypes.Configuration) string {
	ip, err := cfg.GetMyIp()
	if err != nil {
		ip = "unknown"
	}
	return metrics_prefix_impl(s.Name(), ip)
}

func metrics_prefix_impl(name string, ip string) string {
	_ip := "ip-" + strings.Replace(ip, ".", "-", 4)
	return fmt.Sprintf("%s.%s", name, _ip)
}

// Must be done BEFORE any hystrix-related actions
func hystrix_metrics(log *log.Logger, name string) {
	// Hystrix stats collection
	hystrixStreamHandler := hystrix.NewStreamHandler()
	hystrixStreamHandler.Start()

	log.Infof("Serving hystrix stream on %v", constants.DEFAULT_HYSTRIX_ENDPOINT)
	go func() {
		server := &http.Server{
			Addr:    net.JoinHostPort("", constants.DEFAULT_HYSTRIX_ENDPOINT),
			Handler: hystrixStreamHandler,
		}
		graceful.ListenAndServe(server, constants.SHUTDOWN_PERIOD/2)
	}()
}

// Serve a service. See the readme for a template.
func Serve(s Service) {
	ctx := context.NewTaskCtx()
	di := dependencies.NewContainer()

	l := ctx.Logger()

	hystrix_metrics(l, s.Name())

	e := echo.New()

	e.Pre(middleware.Rewrite(map[string]string{
		fmt.Sprintf("/%s/*", s.Name()): "/$1",
	}))

	e.Use(echoxray.Handler(s.Name()))
	e.Use(middleware.RequestID())
	e.Use(log.Recover())

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowCredentials: true,
		AllowHeaders:     []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAuthorization},
		ExposeHeaders:    []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderLocation, echo.HeaderAuthorization},
		AllowOrigins:     []string{"*"},
	}))

	l.Infof("Bootstrapping %v", s.Name())
	cs, err := s.Configurators(ctx)
	if err != nil {
		l.Errorf("Configurators could not be initialized: %v\n", err)
	}
	cf := config.NewConfiguratorWithLogger(ctx.Logger(), cs...)

	// Retrieve the loglevel from consul and update the ctx logger when it changes
	dll := cf.GetString(fmt.Sprintf("%v/%v", s.Name(), constants.DEFAULT_LOG_KEY), constants.DEFAULT_LOG_LEVEL)
	l.SetLevel(dll.Value())
	dll.OnChange(func(path configtypes.Path, value string) {
		l.SetLevel(value)
	})

	err = s.ApplyConfig(ctx, e)
	if err != nil {
		l.Errorf("Failed to apply condiguration. Reason: %v", err)
		return
	}
	l.Debugf("Configuration applied.")

	cfg := frameworkconfig.NewFrameworkConfig(s.Name(), cf)
	di.SetConfiguration(cfg)

	ip, err := cfg.GetMyIp()
	if err != nil {
		l.Errorf("Can't initialize metrics: %v", err)
	}
	if err := echoxray.Configure(xray.Config{
		DaemonAddr:             fmt.Sprintf("%s:2000", ip),
		ServiceVersion:         s.Version(),
		ContextMissingStrategy: ctxmissing.NewDefaultLogErrorStrategy(),
	}); err != nil {
		l.Errorf("Can't initialize xray: %v", err)
	}

	l.Infof("Registering middleware...")

	reqsLimit := cfg.GetPrefixedInt(constants.DEFAULT_REQUEST_RATE, 50).Value() // read the max requests per second from the config

	middleware := []frameworktypes.MiddlewareFunc{
		authenticationHandler(cfg.ServiceNamespace(), cfg.GetPrefixedString(constants.DEFAULT_AUTHENTICATION_ENDPOINT, "")),
		frameworktypes.LimitMiddleware(reqsLimit),
	}

	l.Infof("Registering dependencies...")
	err = s.RegisterDependencies(ctx, di)
	if err != nil {
		l.Errorf("Failed to register dependencies. Reason: %v", err)
		return
	}

	// NOTE store the authorization handler in the DI container, to support the custom authorization scenario
	authHandler := authorizationHandler(cfg)

	di.Set(constants.DI_FW_AUTH_HANDLER, authHandler)

	idi := di.Immutable()
	l.Debugf("Registering dependencies finished.")

	l.Debug("Registering /_admin suite...")
	register_admin_routes(ctx, e, s, idi)

	register_static_routes(ctx, e, s)
	l.Debugf("Static routes applied.")

	register_public_handlers(ctx, e, s, idi)
	l.Debugf("Public routes applied.")

	register_protected_handlers(ctx, e, s, idi, middleware, authHandler)
	l.Debugf("Protected routes applied.")

	register_private_handlers(ctx, e, s, idi)
	l.Debugf("Private routes applied.")

	err = s.BeforeServe(ctx, idi)
	if err != nil {
		l.Debugf("BeforeServe hook returned an error: %v", err)
	}

	l.Infof("Meta endpoints prepared, serving on %v", constants.DEFAULT_LISTEN_ENDPOINT)

	if err != nil {
		l.Errorf("Error while attaching hystrix metrics to statsd: %v", err)
	}

	l.Infof("Serving profiling on %v", constants.DEFAULT_PROFILING_ENDPOINT)

	go func() {
		server := echo.New()
		server.Start(net.JoinHostPort("", constants.DEFAULT_PROFILING_ENDPOINT))
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		signals := s.SignalHandlers(ctx)
		for sig := range c {
			ctx.Logger().Debugf("Received signal: %v", sig)
			for _, s := range signals {
				s(ctx, sig)
			}
		}
	}()

	l.Infof("Serving on %v", constants.DEFAULT_LISTEN_ENDPOINT)
	e.Start(constants.DEFAULT_LISTEN_ENDPOINT)
}

func (s *DefaultService) Reload() error {
	// TODO: Implement graceful reloads
	// e.g. https://github.com/facebookgo/grace
	return nil
}

func (s *DefaultService) Version() string {
	return s.version
}

func (s *DefaultService) BuildTime() string {
	return s.build_time
}

func (s *DefaultService) Name() string {
	if s.name != "" {
		return s.name
	}
	return os.Getenv("SERVICE_NAME")
}

func (s *DefaultService) Configurators(c context.TaskContext) ([]sources.ConfigurationSource, error) {
	c.Logger().Infof("No Configurators() func provided -- using defaults. ")
	localconfig := sources.NewPropertiesSource(constants.DEFAULT_LOCAL_PROPERTIES_FILE)
	cfg := config.NewConfiguratorWithLogger(c.Logger(), localconfig)

	local := cfg.GetBool(constants.DEFAULT_LOCAL_MODE_KEY, false)

	if local.Value() {
		c.Logger().Infof("Local mode detected -- NOT registering consul")
		return []sources.ConfigurationSource{localconfig}, nil
	} else {
		consul := "172.17.0.1:8500"
		c.Logger().Debugf("Registering consul source at '%v'", consul)
		consulclient, err := sources.NewConsulSource(consul)
		return []sources.ConfigurationSource{localconfig, consulclient}, err
	}
}

func (s *DefaultService) ApplyConfig(c context.TaskContext, e *echo.Echo) error {
	c.Logger().Info("No ApplyConfig() func provided")
	return nil
}

func (s *DefaultService) RegisterDependencies(c context.TaskContext, di frameworktypes.MutableContainer) error {
	c.Logger().Info("No RegisterDependencies() func provided")
	return nil
}

func (s *DefaultService) StaticHandlers(c context.TaskContext) map[string]string {
	c.Logger().Info("No StaticHandler() func provided")
	return map[string]string{
		"/apidocs": "api/current/",
	}
}

// NOTE: THIS IS DONE BY CHECKING IF THE HEADER X-Forward-For is missing.
func (s *DefaultService) PrivateHandlers(c context.TaskContext, r frameworktypes.Router) {
	c.Logger().Info("No PrivateHandlers() func provided")
}

func (s *DefaultService) PublicHandlers(c context.TaskContext, r frameworktypes.Router) {
	c.Logger().Info("No PublicHandlers() func provided")
}

func (s *DefaultService) ProtectedHandlers(c context.TaskContext, r frameworktypes.SecuredRouter) {
	c.Logger().Info("No ProtectedHandlers() func provided")
}

func (s *DefaultService) Smokers(c context.TaskContext, di frameworktypes.Container) (*smoke.Smoker, []*smoke.Smoker) {
	c.Logger().Info("No Smokers() func provided")
	return nil, nil
}

func (s *DefaultService) BeforeServe(c context.TaskContext, di frameworktypes.Container) error {
	c.Logger().Info("No BeforeServe() hook provided")
	return nil
}

func (s *DefaultService) SignalHandlers(c context.TaskContext) []frameworktypes.SignalHandler {
	c.Logger().Debug("No SignalHandlers() hook provided")
	return []frameworktypes.SignalHandler{
		func(c context.TaskContext, s os.Signal) {
			c.Logger().Info("Received a ctrl-C -- exitting")
			os.Exit(0)
		},
	}
}

func register_static_routes(ctx context.TaskContext, e *echo.Echo, s Service) {
	for route, path := range s.StaticHandlers(ctx) {
		ctx.Logger().Debugf("Registering static path %v with dir %v", route, path)
		e.Static(route, path)
	}
}

func register_admin_routes(ctx context.TaskContext, e *echo.Echo, s Service, di frameworktypes.Container) {
	r := router.NewPrivateRouter()
	admin.RegisterAdminSuite(&admin.AdminConfig{
		Router: r,
	})
	r.Apply(ctx.Logger(), e, s.Name(), di)
}

func register_public_handlers(ctx context.TaskContext, e *echo.Echo, s Service, di frameworktypes.Container) {
	r := router.NewRouter()

	r.Get("/health", health.HealthHandler(s.Name(), s.Version(), s.BuildTime()))

	self, children := s.Smokers(ctx, di)
	ip, err := di.Configuration().GetMyIp()
	if err != nil {
		ctx.Logger().Errorf("Can't get the ip address of this host: %v", err)
		ip = "unknown"
	}
	r.Get("/smoketest", smoke.SmokeTest(e, s.Name(), s.Version(), s.BuildTime(), ip, self, children))

	s.PublicHandlers(ctx, r)
	r.Apply(ctx.Logger(), e, s.Name(), di)
}

func register_protected_handlers(ctx context.TaskContext, e *echo.Echo, s Service, di frameworktypes.Container, gmw []frameworktypes.MiddlewareFunc, pepf pep.AuthorizationHandlerFunc) {
	r := router.NewSecuredRouter(gmw, pepf)
	s.ProtectedHandlers(ctx, r)
	r.Apply(ctx.Logger(), e, s.Name(), di)
}

func register_private_handlers(ctx context.TaskContext, e *echo.Echo, s Service, di frameworktypes.Container) {
	r := router.NewPrivateRouter()
	s.PrivateHandlers(ctx, r)
	r.Apply(ctx.Logger(), e, s.Name(), di)
}
