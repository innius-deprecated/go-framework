package smoke

import (
	"net/http"
	"testing"

	"bitbucket.org/to-increase/go-context"
	"github.com/stretchr/testify/assert"
	"time"
)

func TestKindle(t *testing.T) {
	deps := []*Smoker{
		{"first", func(c context.SvcContext, name string) (*Smoke, error) {
			return WhiteSmoke(name)
		}},
		{"second", func(c context.SvcContext, name string) (*Smoke, error) {
			return BlackSmoke(name, "Failed to access dependent service XYZ")
		}},
		{"third", func(c context.SvcContext, name string) (*Smoke, error) {
			return WhiteSmoke(name)
		}},
	}
	top := &Smoker{"Top", func(c context.SvcContext, name string) (*Smoke, error) {
		return WhiteSmoke(name)
	}}

	c := context.NewCtx()
	smoke, err := Kindle(c, top, deps)

	assert.Nil(t, err)
	assert.NotNil(t, smoke)
	assert.Equal(t, http.StatusServiceUnavailable, smoke.Status)
	assert.Equal(t, 3, len(smoke.Dependents))
	assert.Equal(t, http.StatusOK, smoke.Dependents[0].Status)
	assert.Equal(t, http.StatusServiceUnavailable, smoke.Dependents[1].Status)
	assert.Equal(t, http.StatusOK, smoke.Dependents[2].Status)
}

func TestHappySmoker(t *testing.T) {
	ctx := context.NewCtx()
	s := &Smoker{
		Name: "happy",
		Smokefun: func(c context.SvcContext, name string) (*Smoke, error) {
			return WhiteSmoke("happy")
		},
	}
	state, err := s.Kindle(ctx)
	assert.Nil(t, err)
	assert.Equal(t, "happy", state.Name)
}

func TestPanicMainSmoker(t *testing.T) {
	ctx := context.NewCtx()
	s := &Smoker{
		Name: "happy",
		Smokefun: func(c context.SvcContext, name string) (*Smoke, error) {
			panic("boom")
		},
	}
	state, err := s.Kindle(ctx)
	assert.Nil(t, err)
	assert.Equal(t, "happy", state.Name)
}

func TestPanicChildSmoker(t *testing.T) {
	ctx := context.NewCtx()
	s1 := &Smoker{
		Name: "abc",
		Smokefun: func(c context.SvcContext, name string) (*Smoke, error) {
			return WhiteSmoke("abc")
		},
	}
	s2 := &Smoker{
		Name: "happy",
		Smokefun: func(c context.SvcContext, name string) (*Smoke, error) {
			panic("boom")
		},
	}
	state, err := Kindle(ctx, s1, []*Smoker{s2})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusServiceUnavailable, state.Status)
}

func TestHibernatingSmoker(t *testing.T) {
	ctx := context.NewCtx()
	s1 := &Smoker{
		Name: "abc",
		Smokefun: func(c context.SvcContext, name string) (*Smoke, error) {
			return WhiteSmoke("abc")
		},
	}
	s2 := &Smoker{
		Name: "sleepy",
		Smokefun: func(c context.SvcContext, name string) (*Smoke, error) {
			return &Smoke{
				Name:    "sleepy",
				Status:  http.StatusOK,
				Message: "About to take a nap",
				TimeStamp: time.Now().Add(-24*100*time.Hour).Unix(),
			}, nil
		},
	}
	state, err := Kindle(ctx, s1, []*Smoker{s2})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusServiceUnavailable, state.Status)
}
