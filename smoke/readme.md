# Smoke 
Simple smoketest package for the Go-based services and their dependencies. 
Terms:
- smoker: the service or component that will emit either white smoke or black smoke
- smoke: the actual smoke that is emitted, containing a http-derived status code (typically 200 and 503) and a timestamped message
- smokefun: the smoke emitting function defined by the smoker
The package is using a handler/function pattern

# Pattern
Each service is required to implement a public smoketest endpoint, which must return a status code and a JSON object containing the smoke.
Dependent services or components (i.e. these services that the current service is depending upon) are exposing a Smoke function.
The smoke functions for the eMaintenance (addable) services are implemented in the go-sdk for each service. Actually a generic Smoker factory method is available in the smoke package.
The smoke functions for external (AWS) services are implemented in the smoke package itself.
The smoke functions assume the availability of a generic context object to be passed in.

# Usage
- The service engineer exposes a 'smoketest' endpoint.
- The service engineer defines a Smoker for the service itself and a series of Smokers for dependent services and components.
- The smoketest endpoint calls smoke.Kindle() with the context, the main smoker and the dependent smokers

# Example
A service X is dependent on servicy Y and Z, and on AWS SNS.
The service defines:
- a Smoker for X: 
	var sx = smoke.Smoker{"X", func(c *context.SvcContext, name string)(*smoke.Smoke, error) {
		return smoke.WhiteSmoke(name)
	}}
- a slice of smokers for Y, Z and SNS: 
	var sm = []smoke.Smoker{
		{"Y", func(c *context.SvcContext, name string)(*smoke.Smoke, error) {
			return client_for_Y.Smoke()
		}},
		{"Z", func(c *context.SvcContext, name string)(*smoke.Smoke, error) {
			return client_for_Z.Smoke()
		}},
		{"Sns", func(c *context.SvcContext, name string)(*smoke.Smoke, error) {
			return smoke.Sns()
		}},
	}

- the implementation of the smoketest handler is simple:
	func smoketest(c *context.SvcContext) error {
		res, _ := smoke.Kindle(c, sx, sm)

		return c.JSON(res.Status, res)
	}
