package smoke

import (
	"bitbucket.org/to-increase/go-context"
	"bitbucket.org/to-increase/go-framework/util"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

/* Get a smoker for a given service. It assumes that each service exposes a smoketest endpoint that returns a status and a JSON document
* @param servicename the name of the service
* @returns a smoker object
 */
func getSmoker(servicename string) *Smoker {
	return &Smoker{
		Name: servicename,
		Smokefun: func(c context.SvcContext, name string) (*Smoke, error) {
			uri, err := util.GetServiceURL(name)
			if err != nil {
				e := fmt.Sprintf("Failed to retrieve service url for %v. Error: %v", servicename, err)
				return BlackSmoke(servicename, e)
			}

			smoke, err := getSmokeFromUri(fmt.Sprintf("%v/smoketest", uri.String()))
			if err != nil || smoke == nil {
				return BlackSmoke(servicename, err.Error())
			}

			return smoke, nil
		},
	}
}

// Do an actual smoke test, including its unmarshalling
func getSmokeFromUri(uri string) (*Smoke, error) {
	client := http.Client{
		Timeout: 1 * time.Second,
	}
	res, err := client.Get(uri)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	decoder := json.NewDecoder(res.Body)
	var s Smoke
	err = decoder.Decode(&s)
	return &s, err
}
