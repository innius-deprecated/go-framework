package smoke

import (
	"fmt"
	"net/http"
	"strings"

	"bitbucket.org/to-increase/go-context"
	"github.com/benschw/dns-clb-go/clb"
	"github.com/valyala/fasthttp"
)

func getConsulMasterAddress() (string, error) {
	c := clb.New()
	addr, err := c.GetAddress("consul.service.consul")
	if err != nil {
		return "", err
	} else if addr.Address == "" {
		return "", fmt.Errorf("No consul master address found")
	} else {
		return addr.Address, nil
	}
}

func ConsulMaster() *Smoker {
	name := "consul-master"
	return &Smoker{
		Name: name,
		Smokefun: func(c context.SvcContext, name string) (*Smoke, error) {
			addr, err := getConsulMasterAddress()
			if err != nil {
				return BlackSmoke(name, fmt.Sprintf("An error occurred while retrieving consul agent data: %v", err.Error()))
			}
			uri := fmt.Sprintf("http://%v:8500/v1/status/leader", addr)
			code, body, err := fasthttp.Get(nil, uri)
			if err != nil {
				return BlackSmoke(name, fmt.Sprintf("An error occured while trying to talk to the consul master: %v", err.Error()))
			}
			if code != http.StatusOK {
				return BlackSmoke(name, fmt.Sprintf("No known consul master (http status: %v)", code))
			}
			response := string(body)
			if response == "" || strings.HasPrefix(response, "No known") {
				return BlackSmoke(name, fmt.Sprintf("No known consul master (response: %v)", response))
			} else {
				return WhiteSmoke(name)
			}
		},
	}
}

func ConsulAgent() *Smoker {
	name := "consul-agent"
	return &Smoker{
		Name: name,
		Smokefun: func(c context.SvcContext, name string) (*Smoke, error) {
			_, err := getConsulMasterAddress()
			if err != nil {
				return BlackSmoke(name, fmt.Sprintf("An error occurred while retrieving consul agent data: %v", err.Error()))
			} else {
				return WhiteSmoke(name)
			}
		},
	}
}
