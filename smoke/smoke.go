package smoke

import (
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/to-increase/go-context"
	"bitbucket.org/to-increase/go-framework/constants"
	log "bitbucket.org/to-increase/go-logger"
)

type (
	/*
	* The definition of a smoker: a component that can determine its status, and give white or black smoke (http status 200 and 503)
	 */
	Smoker struct {
		Name     string
		Smokefun SmokeFun
	}
	/*
	* The function that performs the smoke test
	 */
	SmokeFun func(c context.SvcContext, name string) (*Smoke, error)

	/*
	* The result of the smoke test
	 */
	Smoke struct {
		Name       string   `json:"name"`
		Version    string   `json:"version"`
		BuildTime  string   `json:"buildtime"`
		Status     int      `json:"status"`
		Message    string   `json:"message"`
		HostName   string   `json:"hostname"`
		IpAddress  string   `json:"ipaddress,omitempty"`
		TimeStamp  int64    `json:"timestamp"`
		Dependents []*Smoke `json:"dependents, omitempty"`
	}
)

/*
* A smoker kindles his smoke test
* @param c the global context (currently the echo context)
* @param smokes the results of the dependent smokers
 */
func (s *Smoker) Kindle(c context.SvcContext, smokes ...*Smoke) (res *Smoke, err error) {
	defer func() {
		if err := recover(); err != nil {
			res = &Smoke{
				Name:    s.Name,
				Status:  http.StatusServiceUnavailable,
				Message: fmt.Sprintf("A smoker for %v panicked: %v", s.Name, err),
			}
		}
	}()
	log.Debugf("Performing smoketest on %v", s.Name)

	res, err = s.Smokefun(c, s.Name)
	if err != nil && res == nil {
		res = &Smoke{
			Name:    s.Name,
			Status:  http.StatusServiceUnavailable,
			Message: fmt.Sprintf("Failed to perform smoke test on %v. Error: %v", s.Name, err),
		}
	}

	if len(smokes) > 0 {
		res.Dependents = smokes
		now := time.Now().Unix()
		// propagate status of dependent services up
		for _, s := range smokes {
			if s.Status != http.StatusOK {
				res.Status = s.Status
			}
			//Check if smoketest of dependent service has not gone stale. Timestamp = 0 occurs only in aws dependencies.
			delta := now - s.TimeStamp
			if s.TimeStamp != 0 && (float64(delta) > 2*constants.SMOKE_INTERVAL.Seconds()) {
				res.Status = http.StatusServiceUnavailable
				res.Message = fmt.Sprint("Check expired (executed %s ago); original message: %s", delta, res.Message)
			}
		}
	}

	log.Debugf("Smoketest on %v resulted in %v", s.Name, res)
	return res, err
}

/*
* A smoke test for a smoker and its dependents
* @param c the global context
* @param smoker the main component
* @param smokers the slice with the dependent smokers
 */
func Kindle(c context.SvcContext, smoker *Smoker, smokers []*Smoker) (*Smoke, error) {
	if smoker == nil {
		return nil, fmt.Errorf("Main smoker is nil, cannot kindle")
	}
	// run the dependent smoke functions
	smokes := make([]*Smoke, 0, len(smokers))
	for _, s := range smokers {
		res, _ := s.Kindle(c)
		smokes = append(smokes, res)
	}
	// run the main smoke function
	return smoker.Kindle(c, smokes...)
}

/*
* Generate white smoke, time-stamped
 */
func WhiteSmoke(name string) (*Smoke, error) {
	return &Smoke{
		Name:    name,
		Status:  http.StatusOK,
		Message: fmt.Sprintf("%s. Service %s is available.", time.Now().Format(time.UnixDate), name),
	}, nil
}

/*
* Generate black smoke, time stamped, optionally with a message
 */
func BlackSmoke(name string, msg string) (*Smoke, error) {
	if msg == "" {
		msg = "Service is temporarily unavailable."
	}
	return &Smoke{
		Name:    name,
		Status:  http.StatusServiceUnavailable,
		Message: fmt.Sprintf("%s - %s - %s", name, msg, time.Now().Format(time.UnixDate)),
	}, nil
}
