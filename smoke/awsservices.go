package smoke

import (
	"fmt"
	"net/http"

	"bitbucket.org/to-increase/go-context"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/elasticache"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-sdk-go/service/sqs"
)

func Sns(topic string) *Smoker {
	return &Smoker{
		Name: "SNS",
		Smokefun: func(c context.SvcContext, name string) (*Smoke, error) {
			return snsSmoker(topic)
		},
	}
}

func snsSmoker(topic string) (*Smoke, error) {
	client := sns.New(session.New(), &aws.Config{})
	name := fmt.Sprintf("SNS-%v", topic)

	_, err := client.GetTopicAttributes(&sns.GetTopicAttributesInput{TopicArn: &topic})
	if err != nil {
		return &Smoke{
			Name:    name,
			Status:  http.StatusServiceUnavailable,
			Message: fmt.Sprintf("SNS could not be accessed for topic %s. Error: %v", topic, err),
		}, nil
	} else {
		return &Smoke{
			Name:    name,
			Status:  http.StatusOK,
			Message: fmt.Sprintf("SNS can be accessed for topic %s", topic),
		}, nil
	}
}

func Sqs(queueName string) *Smoker {
	return &Smoker{
		Name: "SQS",
		Smokefun: func(c context.SvcContext, name string) (*Smoke, error) {
			return sqsSmoker(queueName)
		},
	}
}

func sqsSmoker(queueName string) (*Smoke, error) {
	client := sqs.New(session.New(), &aws.Config{})
	name := fmt.Sprintf("SQS-%v", queueName)

	input := &sqs.GetQueueUrlInput{
		QueueName: aws.String(queueName),
	}

	_, err := client.GetQueueUrl(input)
	if err != nil {
		return &Smoke{
			Name:    name,
			Status:  http.StatusServiceUnavailable,
			Message: fmt.Sprintf("SQS could not be accessed for queue %s. Error: %v", queueName, err),
		}, nil
	}

	return &Smoke{
		Name:    name,
		Status:  http.StatusOK,
		Message: fmt.Sprintf("SQS can be accessed for queue %s", queueName),
	}, nil
}

func Dynamo(table string) *Smoker {
	return &Smoker{
		Name: "DynamoDB",
		Smokefun: func(c context.SvcContext, name string) (*Smoke, error) {
			return dynamoSmoker(table)
		},
	}
}

func dynamoSmoker(table string) (*Smoke, error) {
	client := dynamodb.New(session.New(), &aws.Config{})
	name := fmt.Sprintf("DynamoDB-%v", table)

	_, err := client.DescribeTable(&dynamodb.DescribeTableInput{TableName: &table})
	if err != nil {
		return &Smoke{
			Name:    name,
			Status:  http.StatusServiceUnavailable,
			Message: fmt.Sprintf("DynamoDB could not be accessed for table %s. Error: %v", table, err),
		}, nil
	} else {

		return &Smoke{
			Name:    name,
			Status:  http.StatusOK,
			Message: fmt.Sprintf("DynamoDB can be accessed for tables %v", table),
		}, nil
	}
}

func ElastiCache(cacheClusterId string) *Smoker {
	return &Smoker{
		Name: "ElastiCache",
		Smokefun: func(c context.SvcContext, name string) (*Smoke, error) {
			return elasticacheSmoker(cacheClusterId)
		},
	}
}

func elasticacheSmoker(cacheClusterId string) (*Smoke, error) {
	client := elasticache.New(session.New(), &aws.Config{})
	name := fmt.Sprintf("ElastiCache-%v", cacheClusterId)

	res, err := client.DescribeCacheClusters(&elasticache.DescribeCacheClustersInput{CacheClusterId: &cacheClusterId})
	if err != nil {
		return &Smoke{
			Name:    name,
			Status:  http.StatusServiceUnavailable,
			Message: fmt.Sprintf("ElastiCache cluster %s not be accessed. Error: %v", cacheClusterId, err),
		}, nil
	}
	if len(res.CacheClusters) > 0 {
		c := res.CacheClusters[0]
		if *c.CacheClusterStatus != "available" {
			return &Smoke{
				Name:    name,
				Status:  http.StatusServiceUnavailable,
				Message: fmt.Sprintf("ElastiCache cluster %s can be accessed but is not available", cacheClusterId),
			}, nil
		}
	}

	return &Smoke{
		Name:    name,
		Status:  http.StatusOK,
		Message: fmt.Sprintf("ElastiCache cluster %s can be accessed and is available", cacheClusterId),
	}, nil
}
