package smoke

import (
	mb "bitbucket.org/to-increase/go-mountebank"
	"fmt"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestSimpleSmoke(t *testing.T) {
	statussvc := `{"name":"status-service","version":"86b8b5b0e147c47b1112bb88e70e54be948269c3","buildtime":"2016-05-04T12:03:47.734Z","status":500,"message":"No smokers defined.","hostname":"7c50a286109e","timestamp":1462365714883}`
	imp, err := mb.DefaultGetImposter("/smoketest", statussvc, http.StatusOK)
	assert.Nil(t, err, "mb should be running")
	uri := fmt.Sprintf("http://localhost:%v/smoketest", imp.Port)
	s, e := getSmokeFromUri(uri)
	assert.Nil(t, e)
	assert.Equal(t, len(s.Dependents), 0)
	assert.Equal(t, s.Name, "status-service")
	assert.Equal(t, s.TimeStamp, int64(1462365714883))
}

func TestComplexSmoke(t *testing.T) {
	complex := `
{
  "name": "logs-service",
  "version": "3318f1b8eb92eca6afd49d9b2f007d8e780836d4",
  "buildtime": "2016-05-03T22:58:21.970Z",
  "status": 500,
  "message": "A downstream service seems to be broken.",
  "hostname": "2bcb9ac7d8f9",
  "timestamp": 1462366749057,
  "dependents": [
    {
      "name": "dynamodb",
      "version": "",
      "buildtime": "",
      "status": 200,
      "message": "xenon-logs-service-LogEntriesTable-H55N2A6B5YQ",
      "hostname": "",
      "timestamp": 1462366748944
    },
    {
      "name": "authentication-service",
      "version": "f9c888c07dd3ca4b0063eb28defb083196a69e0a",
      "buildtime": "2016-05-03_22:15:43",
      "status": 200,
      "message": "Wed May  4 12:59:08 UTC 2016. Service authentication-service is available.",
      "hostname": "e8ec5861010c",
      "timestamp": 1462366748,
      "dependents": [
        {
          "name": "DynamoDB",
          "version": "",
          "buildtime": "",
          "status": 200,
          "message": "DynamoDB can be accessed for tables [xenon-authentication-service-TokenDynamoDbTable-1LG7M3H2L48XL]",
          "hostname": "",
          "timestamp": 0,
          "dependents": null
        }
      ]
    },
    {
      "name": "authorization-service",
      "version": "b585bd7c8828f7600dc1cd82b379ad933be7ca6c",
      "buildtime": "2016-05-03_22:22:03",
      "status": 503,
      "message": "Wed May  4 12:59:08 UTC 2016. Service authorization-service is available.",
      "hostname": "a751311f20e0",
      "timestamp": 1462366748,
      "dependents": [
        {
          "name": "DynamoDB",
          "version": "",
          "buildtime": "",
          "status": 200,
          "message": "DynamoDB can be accessed for tables [xenon-authorization-service-SecurityGroupsTable-WLR61CN3BF58]",
          "hostname": "",
          "timestamp": 0,
          "dependents": null
        },
        {
          "name": "DynamoDB",
          "version": "",
          "buildtime": "",
          "status": 200,
          "message": "DynamoDB can be accessed for tables [xenon-authorization-service-StatementsTable-1SOLVXS1H6VGA]",
          "hostname": "",
          "timestamp": 0,
          "dependents": null
        },
        {
          "name": "SQS",
          "version": "",
          "buildtime": "",
          "status": 200,
          "message": "SQS can be accessed for queue xenon-authorization-service-EventSQSQueue-16BYGOW111XTR",
          "hostname": "",
          "timestamp": 0,
          "dependents": null
        },
        {
          "name": "SNS",
          "version": "",
          "buildtime": "",
          "status": 503,
          "message": "SNS could not be accessed for topic arn:aws:sns:us-east-1:831844703282:xenon-authorization-service-DomainEventsSNSTopic2-ENF6603JARW2. Error: AuthorizationError: User: arn:aws:sts::831844703282:assumed-role/xenon-authorization-servi-AuthenticationServiceRol-DXMU0O0GLN6V/i-7dcc20e7 is not authorized to perform: SNS:GetTopicAttributes on resource: arn:aws:sns:us-east-1:831844703282:xenon-authorization-service-DomainEventsSNSTopic2-ENF6603JARW2 status code: 403, request id: bb7bbc44-9fb6-5928-a530-f3e76beaddda",
          "hostname": "",
          "timestamp": 0,
          "dependents": null
        }
      ]
    },
    {
      "name": "snsTopic",
      "version": "",
      "buildtime": "",
      "status": 200,
      "message": "arn:aws:sns:us-east-1:831844703282:xenon-logs-service-LogReceivedEventSNSTopic2-14J9SFDAV7LZN",
      "hostname": "",
      "timestamp": 1462366749050
    },
    {
      "name": "snsTopicSubscription",
      "version": "",
      "buildtime": "",
      "status": 200,
      "message": "arn:aws:sns:us-east-1:831844703282:xenon-logs-service-LogReceivedEventSNSTopic2-14J9SFDAV7LZN",
      "hostname": "",
      "timestamp": 1462366749057
    },
    {
      "name": "snsTopicSubscription",
      "version": "",
      "buildtime": "",
      "status": 200,
      "message": "arn:aws:sns:us-east-1:831844703282:xenon-logs-service-LogReceivedEventSNSTopic2-14J9SFDAV7LZN",
      "hostname": "",
      "timestamp": 1462366749057
    }
  ]
}
	`
	imp, _ := mb.DefaultGetImposter("/smoketest", complex, http.StatusOK)
	uri := fmt.Sprintf("http://localhost:%v/smoketest", imp.Port)
	s, e := getSmokeFromUri(uri)
	assert.Nil(t, e)
	assert.Equal(t, len(s.Dependents), 6)
	assert.Equal(t, s.Name, "logs-service")
	assert.Equal(t, s.Status, 500)
	assert.Equal(t, s.Dependents[0].Name, "dynamodb")
	assert.Equal(t, s.Dependents[0].Message, "xenon-logs-service-LogEntriesTable-H55N2A6B5YQ")
}
