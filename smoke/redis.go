package smoke

import (
	"fmt"

	"bitbucket.org/to-increase/go-cache"
	context "bitbucket.org/to-increase/go-context"
)

// checks the connection to a redis cluster and ping the endpoint
func Redis(clusterName string) *Smoker {
	name := fmt.Sprintf("redis-cache %s", clusterName)
	return &Smoker{
		Name: name,
		Smokefun: func(c context.SvcContext, name string) (*Smoke, error) {
			cl := cache.NewCacheClient(clusterName, 0)
			err := cl.Ping()
			if err != nil {
				return BlackSmoke(name, fmt.Sprintf("could not ping the redis cluster: %v", err.Error()))
			}
			return WhiteSmoke(name)
		}}
}
