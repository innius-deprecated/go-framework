package smoke

import "bitbucket.org/to-increase/go-framework/services"

func AnalyticsEventService() *Smoker {
	return createSmoker(services.ANALYTICS_EVENT_SERVICE)
}

func AnalyticsManagementService() *Smoker {
	return createSmoker(services.ANALYTICS_MANAGEMENT_SERVICE)
}

func AnalyticsQueryService() *Smoker {
	return createSmoker(services.ANALYTICS_QUERY_SERVICE)
}

// Create a smoker that verifies whether the authentication-service is alive
func AuthenticationService() *Smoker {
	return createSmoker(services.AUTHENTICATION_SERVICE)
}

// Create a smoker that verifies whether the authorization-service is alive
func AuthorizationService() *Smoker {
	return createSmoker(services.AUTHORIZATION_SERVICE)
}

func CollaborationService() *Smoker {
	return createSmoker(services.COLLABORATION_SERVICE)
}

func CollateralService() *Smoker {
	return createSmoker(services.COLLATERAL_SERVICE)
}

func ConnectionService() *Smoker {
	return createSmoker(services.CONNECTION_SERVICE)
}

func DemomachineService() *Smoker {
	return createSmoker(services.DEMOMACHINE_SERVICE)
}

func DevopsDashboardService() *Smoker {
	return createSmoker(services.DEVOPS_DASHBOARD_SERVICE)
}

func ElasticSearchService() *Smoker {
	return createSmoker(services.ELASTICSEARCH)
}

func FeedbackService() *Smoker {
	return createSmoker(services.FEEDBACK_SERVICE)
}

func GraphiteService() *Smoker {
	return createSmoker(services.GRAPHITE_SERVICE)
}

func HystrixDashboardService() *Smoker {
	return createSmoker(services.HYSTRIX_DASHBOARD)
}

func IdentificationService() *Smoker {
	return createSmoker(services.IDENTIFICATION_SERVICE)
}

func KibanaService() *Smoker {
	return createSmoker(services.KIBANA)
}

func LogstashShipperService() *Smoker {
	return createSmoker(services.LOGSTASH_SHIPPER)
}

func LogsService() *Smoker {
	return createSmoker(services.LOGS_SERVICE)
}

func MachinePartsService() *Smoker {
	return createSmoker(services.MACHINE_PARTS_SERVICE)
}

func MachineSpecificationService() *Smoker {
	return createSmoker(services.MACHINE_SPECIFICATION_SERVICE)
}

func MessagingService() *Smoker {
	return createSmoker(services.MESSAGING_SERVICE)
}

func MonitoringService() *Smoker {
	return createSmoker(services.MONITORING_SERVICE)
}

func StatusService() *Smoker {
	return createSmoker(services.STATUS_SERVICE)
}

func UploadService() *Smoker {
	return createSmoker(services.UPLOAD_SERVICE)
}

func WebUIAdminService() *Smoker {
	return createSmoker(services.WEB_UI_ADMIN_SERVICE)
}

func createSmoker(service services.MicroService) *Smoker {
	return getSmoker(string(service))
}
