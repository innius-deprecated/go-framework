package smoke

import (
	"fmt"
	"time"

	"bitbucket.org/to-increase/go-context"
	"bitbucket.org/to-increase/go-framework/types"
	"bitbucket.org/to-increase/go-framework/util"
	"github.com/labstack/echo"
)

func SmokeTest(e *echo.Echo, name string, version string, buildtime string, ip string, smoker *Smoker, smokers []*Smoker) types.Handler {
	var result *Smoke = failed(name, version, buildtime, fmt.Errorf("Not executed yet."))
	var err error = nil

	ctx := context.NewCtxFromEchoWithName(name, nil)

	smoke_test := func() {
		result, err = Kindle(ctx, smoker, smokers)
		if err != nil {
			result = failed(name, version, buildtime, err)
		} else {
			result.Version = version
			result.BuildTime = buildtime
			result.HostName = util.GetHostname()
			result.IpAddress = ip
			result.TimeStamp = time.Now().Unix() * 1000 //in ms
		}
	}

	// Spawn the supervisor
	// go func() {
	// 	for range time.Tick(constants.SMOKE_INTERVAL) {
	// 		go smoke_test()
	// 	}
	// }()

	// But execute a smoke test straight away (otherwise we have to wait until SMOKE_INTERVAL has passed once.
	smoke_test()

	return func(c context.SvcContext, di types.Container) error {
		return c.JSON(result.Status, result)
	}
}

func failed(name string, version string, buildtime string, err error) *Smoke {
	return &Smoke{
		Name:       name,
		Version:    version,
		BuildTime:  buildtime,
		Status:     500,
		Message:    err.Error(),
		HostName:   util.GetHostname(),
		TimeStamp:  time.Now().Unix() * 1000, //in ms
		Dependents: []*Smoke{},
	}
}
