package router

import (
	"bitbucket.org/to-increase/go-auth/pep/actions"
	"bitbucket.org/to-increase/go-framework/types"
	"bitbucket.org/to-increase/go-logger"
	"github.com/labstack/echo"
)

type method int

const (
	GET method = iota
	PUT
	POST
	DELETE
	OPTIONS
	HEAD
	PATCH
)

func (m *method) String() string {
	switch *m {
	case GET:
		return "get"
	case PUT:
		return "put"
	case POST:
		return "post"
	case DELETE:
		return "delete"
	case OPTIONS:
		return "options"
	case HEAD:
		return "head"
	case PATCH:
		return "patch"
	default:
		return "unknown"
	}
}

type endpoint struct {
	Path     string
	Method   method
	Resource types.Provider
	Action   actions.Action
}

type Route struct {
	Endpoint endpoint
	Route    types.Handler
}

type RouterImpl struct {
	Routes []Route
}

func (r *RouterImpl) set(m method, path string, h types.Handler, action actions.Action, provider types.Provider) {
	r.Routes = append(r.Routes, Route{
		new_endpoint(m, path, action, provider),
		h,
	})
}

func (r *RouterImpl) Get(path string, h types.Handler) {
	r.set(GET, path, h, actions.None, nil)
}

func (r *RouterImpl) Put(path string, h types.Handler) {
	r.set(PUT, path, h, actions.None, nil)
}

func (r *RouterImpl) Post(path string, h types.Handler) {
	r.set(POST, path, h, actions.None, nil)
}

func (r *RouterImpl) Delete(path string, h types.Handler) {
	r.set(DELETE, path, h, actions.None, nil)
}

func (r *RouterImpl) Options(path string, h types.Handler) {
	r.set(OPTIONS, path, h, actions.None, nil)
}

func (r *RouterImpl) Head(path string, h types.Handler) {
	r.set(HEAD, path, h, actions.None, nil)
}

func (r *RouterImpl) Patch(path string, h types.Handler) {
	r.set(PATCH, path, h, actions.None, nil)
}

func (r *RouterImpl) Apply(log *logger.Logger, e *echo.Echo, name string, di types.Container) {
	wrap_di := WrapEchoHandlerWithDIAndMetrics
	wrap_time := WrapEchoHandlerWithTimer

	for _, v := range r.Routes {
		endpoint := v.Endpoint
		log.Debugf("Registering public path %v with method %v", endpoint.Path, endpoint.Method)

		_di := wrap_di(name, di, v)
		_timed := wrap_time(v.Endpoint)(_di)

		switch endpoint.Method {
		case GET:
			e.GET(endpoint.Path, _timed)
		case PUT:
			e.PUT(endpoint.Path, _timed)
		case POST:
			e.POST(endpoint.Path, _timed)
		case DELETE:
			e.DELETE(endpoint.Path, _timed)
		case OPTIONS:
			e.OPTIONS(endpoint.Path, _timed)
		case HEAD:
			e.HEAD(endpoint.Path, _timed)
		case PATCH:
			e.PATCH(endpoint.Path, _timed)
		default:
			log.Errorf("Unknown HTTP method for endpoint %v", endpoint.Path)
		}
	}
}

func NewRouter() *RouterImpl {
	return &RouterImpl{
		Routes: make([]Route, 0, 0),
	}
}

func new_endpoint(m method, path string, action actions.Action, provider types.Provider) endpoint {
	return endpoint{
		Path:     path,
		Method:   m,
		Resource: provider,
		Action:   action,
	}
}
