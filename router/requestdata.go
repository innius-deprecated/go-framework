package router

import (
	"bitbucket.org/to-increase/go-context"
	"bitbucket.org/to-increase/go-framework/types"
)

type requestdata struct {
	ctx context.SvcContext
}

func NewRequestData(c context.SvcContext) types.RequestData {
	return &requestdata{
		ctx: c,
	}
}

func (r *requestdata) Company() string {
	return r.ctx.Claims().Principal.CompanyId
}
func (r *requestdata) User() string {
	return r.ctx.Claims().Principal.UserId
}
func (r *requestdata) Param(param string) string {
	return r.ctx.Param(param)
}

func (r *requestdata) Path() string {
	return r.ctx.Path()
}
