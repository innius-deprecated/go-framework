package router

import (
	"bitbucket.org/to-increase/go-auth/pep/actions"
	"bitbucket.org/to-increase/go-context"
	"bitbucket.org/to-increase/go-framework/types"
	"bitbucket.org/to-increase/go-logger"
	"github.com/labstack/echo"
)

type PrivateRouterImpl struct {
	Routes []Route
}

func (r *PrivateRouterImpl) set(m method, path string, h types.Handler, action actions.Action, provider types.Provider) {
	r.Routes = append(r.Routes, Route{
		new_endpoint(m, path, action, provider),
		h,
	})
}

func (r *PrivateRouterImpl) Get(path string, h types.Handler) {
	r.set(GET, path, h, actions.None, nil)
}

func (r *PrivateRouterImpl) Put(path string, h types.Handler) {
	r.set(PUT, path, h, actions.None, nil)
}

func (r *PrivateRouterImpl) Post(path string, h types.Handler) {
	r.set(POST, path, h, actions.None, nil)
}

func (r *PrivateRouterImpl) Delete(path string, h types.Handler) {
	r.set(DELETE, path, h, actions.None, nil)
}

func (r *PrivateRouterImpl) Options(path string, h types.Handler) {
	r.set(OPTIONS, path, h, actions.None, nil)
}

func (r *PrivateRouterImpl) Head(path string, h types.Handler) {
	r.set(HEAD, path, h, actions.None, nil)
}

func (r *PrivateRouterImpl) Patch(path string, h types.Handler) {
	r.set(PATCH, path, h, actions.None, nil)
}

func (r *PrivateRouterImpl) Apply(log *logger.Logger, e *echo.Echo, name string, di types.Container) {
	private := func(v Route) Route {
		fn := func(c context.SvcContext, container types.Container) error {
			if types.InternalRequest(c) {
				return v.Route(c, container)
			} else {
				return c.String(403, "Internal usage only.")
			}
		}
		return Route{
			Endpoint: v.Endpoint,
			Route:    fn,
		}
	}
	wrap_di := WrapEchoHandlerWithDIAndMetrics
	wrap_time := WrapEchoHandlerWithTimer

	for _, v := range r.Routes {
		endpoint := v.Endpoint
		log.Debugf("Registering private path %v with method %v", endpoint.Path, endpoint.Method)

		_di := wrap_di(name, di, private(v))
		_timed := wrap_time(v.Endpoint)(_di)

		switch endpoint.Method {
		case GET:
			e.GET(endpoint.Path, _timed)
		case PUT:
			e.PUT(endpoint.Path, _timed)
		case POST:
			e.POST(endpoint.Path, _timed)
		case DELETE:
			e.DELETE(endpoint.Path, _timed)
		case OPTIONS:
			e.OPTIONS(endpoint.Path, _timed)
		case HEAD:
			e.HEAD(endpoint.Path, _timed)
		case PATCH:
			e.PATCH(endpoint.Path, _timed)
		default:
			log.Errorf("Unknown HTTP method for private endpoint %v", endpoint.Path)
		}
	}
}

func NewPrivateRouter() *PrivateRouterImpl {
	return &PrivateRouterImpl{
		Routes: make([]Route, 0, 0),
	}
}
