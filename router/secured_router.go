package router

import (
	"bitbucket.org/to-increase/go-auth/pep"
	"bitbucket.org/to-increase/go-auth/pep/actions"
	"bitbucket.org/to-increase/go-context"
	"bitbucket.org/to-increase/go-framework/types"
	"bitbucket.org/to-increase/go-logger"
	"github.com/labstack/echo"
)

func NewSecuredRouter(gmw []types.MiddlewareFunc, pepf pep.AuthorizationHandlerFunc) *SecuredRouterImpl {
	return &SecuredRouterImpl{
		pepf:   pepf,
		gmw:    gmw,
		Routes: make([]Route, 0, 0),
	}
}

type SecuredRouterImpl struct {
	pepf   pep.AuthorizationHandlerFunc
	gmw    []types.MiddlewareFunc
	Routes []Route
}

func (r *SecuredRouterImpl) set(m method, path string, h types.Handler, action actions.Action, provider types.Provider) {
	r.Routes = append(r.Routes, Route{
		new_endpoint(m, path, action, provider),
		h,
	})
}

func (r *SecuredRouterImpl) Get(path string, h types.Handler, action actions.Action, provider types.Provider) {
	r.set(GET, path, h, action, provider)
}

func (r *SecuredRouterImpl) Put(path string, h types.Handler, action actions.Action, provider types.Provider) {
	r.set(PUT, path, h, action, provider)
}

func (r *SecuredRouterImpl) Post(path string, h types.Handler, action actions.Action, provider types.Provider) {
	r.set(POST, path, h, action, provider)
}

func (r *SecuredRouterImpl) Delete(path string, h types.Handler, action actions.Action, provider types.Provider) {
	r.set(DELETE, path, h, action, provider)
}

func (r *SecuredRouterImpl) Options(path string, h types.Handler, action actions.Action, provider types.Provider) {
	r.set(OPTIONS, path, h, action, provider)
}

func (r *SecuredRouterImpl) Head(path string, h types.Handler, action actions.Action, provider types.Provider) {
	r.set(HEAD, path, h, action, provider)
}

func (r *SecuredRouterImpl) Patch(path string, h types.Handler, action actions.Action, provider types.Provider) {
	r.set(PATCH, path, h, action, provider)
}

func (r *SecuredRouterImpl) Apply(log *logger.Logger, e *echo.Echo, name string, di types.Container) {
	wrap_mw := WrapEchoHandlerWithMiddlewareAndMetrics
	wrap_time := WrapEchoHandlerWithTimer

	for _, v := range r.Routes {
		endpoint := v.Endpoint

		var ah types.MiddlewareFunc = func(c context.SvcContext) error {
			if endpoint.Resource == nil {
				return nil
			}

			rd := NewRequestData(c)
			trn, err := endpoint.Resource(rd)

			if err != nil {
				return err
			}

			return r.pepf(c, trn, endpoint.Action)
		}

		mw := append(r.gmw, ah)

		log.Debugf("Registering protected path %v with method %v and action %v", endpoint.Path, endpoint.Method, endpoint.Action)

		_di := wrap_mw(name, di, v, mw)
		_timed := wrap_time(v.Endpoint)(_di)

		switch endpoint.Method {
		case GET:
			e.GET(endpoint.Path, _timed)
		case PUT:
			e.PUT(endpoint.Path, _timed)
		case POST:
			e.POST(endpoint.Path, _timed)
		case DELETE:
			e.DELETE(endpoint.Path, _timed)
		case OPTIONS:
			e.OPTIONS(endpoint.Path, _timed)
		case HEAD:
			e.HEAD(endpoint.Path, _timed)
		case PATCH:
			e.PATCH(endpoint.Path, _timed)
		default:
			log.Errorf("Unknown HTTP method for private endpoint %v", endpoint.Path)
		}
	}
}
