package router

import (
	"fmt"
	"strings"

	"bitbucket.org/to-increase/go-context"
	"bitbucket.org/to-increase/go-framework/types"
	"github.com/labstack/echo"
)

func handler_count(method string, path string) string {
	return fmt.Sprintf("handlers.%s.count", path_as_key(method, path))
}

func handler_mwcount(method string, path string) string {
	return fmt.Sprintf("handlers.%s.mwcount", path_as_key(method, path))
}

func handler_duration(method string, path string) string {
	return fmt.Sprintf("handlers.%s.duration", path_as_key(method, path))
}

func handler_mwduration(method string, path string) string {
	return fmt.Sprintf("handlers.%s.mwduration", path_as_key(method, path))
}

func path_as_key(method, path string) string {
	dashed := []string{}
	for _, s := range strings.Split(path, "/") {
		if s != "" {
			dashed = append(dashed, strings.Trim(s, ":"))
		}
	}
	return fmt.Sprintf("%s.%s", method, strings.Join(dashed, "."))
}

func WrapEchoHandlerWithMiddlewareAndMetrics(name string, di types.Container, route Route, middleware []types.MiddlewareFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		c := context.NewCtxFromEchoWithName(name, ctx)
		for _, f := range middleware {
			if err := f(c); err != nil {
				return err
			}
		}
		return route.Route(c, di)
	}
}

// Same as above, but with DI
func WrapEchoHandlerWithDIAndMetrics(name string, di types.Container, route Route) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		return route.Route(context.NewCtxFromEchoWithName(name, ctx), di)
	}
}

func WrapEchoHandlerWithTimer(ep endpoint) func(echo.HandlerFunc) echo.HandlerFunc {
	return func(h echo.HandlerFunc) echo.HandlerFunc {
		return func(ctx echo.Context) error {
			e := h(ctx)
			return e
		}
	}
}

// Wrap a to-increase service handler in an echo handler, automatically configuring the SvcContext object
// and the middleware; middleware functions are executed prior to the handler function
// TODO: DEPRECATED REMOVE ME
func WrapEchoHandlerWithMiddleware(name string, di types.Container, handler types.Handler, middleware []types.MiddlewareFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		c := context.NewCtxFromEchoWithName(name, ctx)
		for _, f := range middleware {
			if err := f(c); err != nil {
				return err
			}
		}
		return handler(c, di)
	}
}

// Same as above, but with DI
// TODO: DEPRECATED REMOVE ME
func WrapEchoHandlerWithDI(name string, di types.Container, handler types.Handler) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		return handler(context.NewCtxFromEchoWithName(name, ctx), di)
	}
}
