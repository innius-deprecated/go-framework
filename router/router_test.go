package router

import "testing"
import "github.com/stretchr/testify/assert"

func TestMetricPath(t *testing.T) {
	route := &Route{
		Endpoint: endpoint{
			Method: GET,
			Path:   "/:machinetrn/:sensorid/state",
		},
	}
	res := handler_count(route.Endpoint.Method.String(), route.Endpoint.Path)

	assert.Equal(t, "handlers.get.machinetrn.sensorid.state.count", res)
}
