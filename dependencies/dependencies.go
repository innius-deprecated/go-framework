package dependencies

import (
	"bitbucket.org/to-increase/go-framework/types"
)

type depco struct {
	deps   map[string]interface{}
	config types.Configuration
}

func NewContainer() types.MutableContainer {
	return &depco{
		deps:   map[string]interface{}{},
		config: nil,
	}
}

func (c *depco) Configuration() types.Configuration {
	return c.config
}

func (c *depco) SetConfiguration(conf types.Configuration) {
	c.config = conf
}

func (c *depco) Has(name string) bool {
	_, exists := c.deps[name]
	return exists
}

func (c *depco) Get(name string) interface{} {
	return c.deps[name]
}

func (c *depco) All() map[string]interface{} {
	return c.deps
}

func (c *depco) Set(name string, dep interface{}) {
	c.deps[name] = dep
}

func (c *depco) Immutable() types.Container {
	return c
}
