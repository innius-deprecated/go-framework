package dependencies

import (
	"bitbucket.org/to-increase/go-framework/types"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCreateContainer(t *testing.T) {
	var c types.MutableContainer = NewContainer()
	var i types.Container = c.Immutable()
	assert.NotNil(t, c)
	assert.NotNil(t, i)
}

func TestCreateContainerUnknownValue(t *testing.T) {
	c := NewContainer()
	i := c.Immutable()
	assert.False(t, i.Has("xyz"))
	assert.Nil(t, i.Get("xyz"))
}

func TestCreateContainerSetValues(t *testing.T) {
	c := NewContainer()
	c.Set("abc", "def")
	i := c.Immutable()
	assert.True(t, i.Has("abc"))
	assert.False(t, i.Has("xyz"))
	assert.Equal(t, "def", i.Get("abc"))
	assert.Nil(t, i.Get("xyz"))
}
