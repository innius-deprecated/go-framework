package services

type MicroService string

const (
	/**
	 * The Elasticsearch, Graphite, Hystrix dashboard and Logstash shipper constants
	 * are present in go-framework, but not in node-framework. Should be looked at.
	 */
	ANALYTICS_EVENT_SERVICE       MicroService = "analytics-event-service"
	ANALYTICS_INGESTION_SERVICE   MicroService = "analytics-ingestion-service"
	ANALYTICS_MANAGEMENT_SERVICE  MicroService = "analytics-management-service"
	ANALYTICS_QUERY_SERVICE       MicroService = "analytics-query-service"
	AUTHENTICATION_SERVICE        MicroService = "authentication-service"
	AUTHORIZATION_SERVICE         MicroService = "authorization-service"
	COLLABORATION_SERVICE         MicroService = "collaboration-service"
	COLLATERAL_SERVICE            MicroService = "collateral-service"
	CONNECTION_SERVICE            MicroService = "connection-service"
	DEMOMACHINE_SERVICE           MicroService = "demomachine-service"
	DEVOPS_DASHBOARD_SERVICE      MicroService = "devops-dashboard-service"
	ELASTICSEARCH                 MicroService = "elasticsearch-cluster"
	FEEDBACK_SERVICE              MicroService = "feedback-service"
	GRAPHITE_SERVICE              MicroService = "graphite-service"
	HYSTRIX_DASHBOARD             MicroService = "hystrix-dashboard"
	IDENTIFICATION_SERVICE        MicroService = "identification-service"
	KIBANA                        MicroService = "kibana"
	KPI_SERVICE                   MicroService = "kpi-service"
	LOCATION_SERVICE              MicroService = "location-service"
	LOGSTASH_SHIPPER              MicroService = "logstash-shipper"
	LOGS_SERVICE                  MicroService = "logs-service"
	MACHINE_PARTS_SERVICE         MicroService = "parts-service"
	MACHINE_SPECIFICATION_SERVICE MicroService = "specification-service"
	MESSAGING_SERVICE             MicroService = "messaging-service"
	MONITORING_SERVICE            MicroService = "monitoring-service-v2"
	SENSOR_SERVICE                MicroService = "sensor-service"
	STATUS_SERVICE                MicroService = "status-service"
	UPLOAD_SERVICE                MicroService = "upload-service"
	WEB_UI_ADMIN_SERVICE          MicroService = "web-ui-admin-service"
)
