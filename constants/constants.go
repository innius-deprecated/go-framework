package constants

import "time"

const DEFAULT_LISTEN_ENDPOINT string = ":8080"
const DEFAULT_HYSTRIX_ENDPOINT string = "8081"
const DEFAULT_PROFILING_ENDPOINT string = "6060"
const DEFAULT_STATSD_PORT int = 8125

const DEFAULT_LOG_KEY string = "loglevel"
const DEFAULT_LOG_LEVEL string = "info"
const DEFAULT_LOCAL_MODE_KEY string = "local"

const DEFAULT_REQUEST_RATE = "request_rate"

const DEFAULT_AUTHENTICATION_ENDPOINT string = "authentication_endpoint"
const DEFAULT_AUTHORIZATION_ENDPOINT string = "authorization_endpoint"
const DEFAULT_LOCAL_PROPERTIES_FILE string = "config.properties"

const DI_FW_AUTH_HANDLER = "fw_auth_handler"

const SMOKE_INTERVAL = 150 * time.Second
const SHUTDOWN_PERIOD = 10 * time.Second

const AUTHORIZATION_CACHE_TTL = "authorization_cache_ttl"
const AUTHORIZATION_CACHE_CLUSTER = "AuthorizationCacheCluster"
