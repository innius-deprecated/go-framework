package admin

import (
	"fmt"
	"net/http"
	"sort"

	"bitbucket.org/to-increase/go-context"
	"bitbucket.org/to-increase/go-framework/types"
)

type AdminConfig struct {
	Router types.Router
}

func prefix(uri string) string {
	return fmt.Sprintf("/_admin/%s", uri)
}

func RegisterAdminSuite(cfg *AdminConfig) {
	if cfg == nil {
		return
	}

	cfg.Router.Get(prefix("config/keys"), list_keys)
}

func list_keys(ctx context.SvcContext, di types.Container) error {
	subs := di.Configuration().ServiceNamespace().Subscriptions()
	sort.Strings(subs) // sort for convenience

	ret := &Subscriptions{
		Subscriptions: subs,
	}
	return ctx.JSON(http.StatusOK, ret)
}
