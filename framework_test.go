package framework_test

import (
	"encoding/json"
	"net/http"
	"sync"
	"testing"
	"time"

	"bitbucket.org/to-increase/go-context"
	"bitbucket.org/to-increase/go-framework"
	"bitbucket.org/to-increase/go-framework/admin"
	"bitbucket.org/to-increase/go-framework/types"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
)

const internal_request_header_name = "X-Forwarded-For"
const internal_request_header_value = ""

type foo struct {
	*framework.DefaultService
}

var f = foo{
	framework.DefaultServiceInstance,
}

func (f foo) PublicHandlers(c context.TaskContext, r types.Router) {
	r.Get("/", func(c context.SvcContext, di types.Container) error {
		return c.String(http.StatusOK, "bla")
	})
	r.Get("/timeout", func(c context.SvcContext, di types.Container) error {
		time.Sleep(25 * time.Millisecond)
		return c.String(http.StatusOK, "I'm too late")
	})
	r.Get("/biggertimeout", func(c context.SvcContext, di types.Container) error {
		time.Sleep(10 * time.Millisecond)
		return c.String(http.StatusOK, "I'm on time")
	})
	r.Get("/foo", func(c context.SvcContext, di types.Container) error {
		c.Error(errors.New("foo"))
		return nil
	})
	r.Get("/bar", func(c context.SvcContext, di types.Container) error {
		return errors.New("foo")
	})
	return
}

func (f foo) Name() string {
	return "abc"
}

var once sync.Once

var client *http.Client

func DoServe() {
	client = &http.Client{
		Timeout: 500 * time.Millisecond,
	}
	go framework.Serve(f)
	time.Sleep(3 * time.Second)
}

func TestTimeoutHandler(t *testing.T) {
	once.Do(DoServe)
	req, err := http.NewRequest("GET", "http://localhost:8080/timeout", nil)
	if err != nil {
		t.Fatalf("Failed to prepare get: %v", err)
	}

	resp, err := client.Do(req)
	if err != nil {
		t.Fatalf("Failed to execute get: %v", err)
	}
	// timeouts are not handled in framework;
	assert.Equal(t, http.StatusOK, resp.StatusCode)
}

func TestOnTimeHandler(t *testing.T) {
	once.Do(DoServe)
	req, err := http.NewRequest("GET", "http://localhost:8080/biggertimeout", nil)
	if err != nil {
		t.Fatalf("Failed to prepare get: %v", err)
	}

	resp, err := client.Do(req)
	if err != nil {
		t.Fatalf("Failed to execute get: %v", err)
	}
	assert.Equal(t, http.StatusOK, resp.StatusCode)
}

func TestFooErrorHandler(t *testing.T) {
	once.Do(DoServe)
	req, err := http.NewRequest("GET", "http://localhost:8080/foo", nil)
	if err != nil {
		t.Fatalf("Failed to prepare get: %v", err)
	}

	resp, err := client.Do(req)
	if err != nil {
		t.Fatalf("Failed to execute get: %v", err)
	}
	assert.Equal(t, http.StatusInternalServerError, resp.StatusCode)
}

func TestBarErrorHandler(t *testing.T) {
	once.Do(DoServe)
	req, err := http.NewRequest("GET", "http://localhost:8080/bar", nil)
	if err != nil {
		t.Fatalf("Failed to prepare get: %v", err)
	}

	resp, err := client.Do(req)
	if err != nil {
		t.Fatalf("Failed to execute get: %v", err)
	}
	assert.Equal(t, http.StatusInternalServerError, resp.StatusCode)
}

func TestSubscriptionsAdminEndpoint(t *testing.T) {
	once.Do(DoServe)
	req, err := http.NewRequest("GET", "http://localhost:8080/_admin/config/keys", nil)
	req.Header.Add(internal_request_header_name, internal_request_header_value)
	if err != nil {
		t.Fatalf("Failed to prepare get: %v", err)
	}

	resp, err := client.Do(req)
	if err != nil {
		t.Fatalf("Failed to execute get: %v", err)
	}
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	assert.NotNil(t, resp.Body)

	keys := &admin.Subscriptions{}

	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(keys)

	assert.Nil(t, err)
	assert.True(t, len(keys.Subscriptions) > 0)
}
