package types

import (
	"bitbucket.org/to-increase/go-context"
	"github.com/didip/tollbooth"
	"github.com/didip/tollbooth/limiter"
)

// LimitMiddleware returns new middleware for limiting number of requests per ip address
func LimitMiddleware(reqsLimit int) MiddlewareFunc {

	limiter := tollbooth.NewLimiter(float64(reqsLimit), nil)
	limiter.SetIPLookups([]string{"X-Forwarded-For", "X-Real-IP", "RemoteAddr"})
	limiter.SetForwardedForIndexFromBehind(100)
	return limitMiddleware(limiter)
}

func limitMiddleware(limiter *limiter.Limiter) MiddlewareFunc {
	return func(c context.SvcContext) error {
		// if !InternalRequest(c) {
		// 	httpError := tollbooth.LimitByRequest(limiter, c.Response(), c.Request())
		// 	if httpError != nil {
		// 		return echo.NewHTTPError(httpError.StatusCode, httpError.Message)
		// 	}
		// }
		return nil
	}
}
