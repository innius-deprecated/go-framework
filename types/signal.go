package types

import (
	"bitbucket.org/to-increase/go-context"
	"os"
)

type SignalHandler func(context.TaskContext, os.Signal)
