package types

type Container interface {
	// Retrieve dynamic configurations
	Configuration() Configuration
	// Check whether this container has a dependency available.
	Has(string) bool
	// Retrieve a dependency by name.
	Get(string) interface{}
	// Retrieve all dependencies
	All() map[string]interface{}
}

type MutableContainer interface {
	// Set a configurator
	SetConfiguration(Configuration)
	// Return a Container ref, which is immutable.
	Immutable() Container
	// Set a dependency in the container.
	Set(string, interface{})

	// Retrieve dynamic configurations
	Configuration() Configuration
	// Check whether this container has a dependency available.
	Has(string) bool
	// Retrieve a dependency by name.
	Get(string) interface{}
	// Retrieve all dependencies
	All() map[string]interface{}
}
