package types

import (
	"bitbucket.org/to-increase/go-context"
)

type Handler func(c context.SvcContext, container Container) error

type MiddlewareFunc func(c context.SvcContext) error

type RequestData interface {
	Company() string
	User() string
	Param(string) string
	Path() string
}

// InternalRequest checks if the request is a service - service request
func InternalRequest(c context.SvcContext) bool {
	return c.Request().Header.Get("X-Forwarded-For") == ""
}
