package types

import (
	"bitbucket.org/to-increase/go-auth/pep/actions"
	"bitbucket.org/to-increase/go-trn"
)

type Provider func(RequestData) (*trn.TRN, error)

type Router interface {
	Get(string, Handler)
	Put(string, Handler)
	Post(string, Handler)
	Delete(string, Handler)
	Options(string, Handler)
	Head(string, Handler)
	Patch(string, Handler)
}

type SecuredRouter interface {
	Get(string, Handler, actions.Action, Provider)
	Put(string, Handler, actions.Action, Provider)
	Post(string, Handler, actions.Action, Provider)
	Delete(string, Handler, actions.Action, Provider)
	Options(string, Handler, actions.Action, Provider)
	Head(string, Handler, actions.Action, Provider)
	Patch(string, Handler, actions.Action, Provider)
}
