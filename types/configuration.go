package types

import (
	"bitbucket.org/to-increase/go-config"
	"bitbucket.org/to-increase/go-config/types"
)

type Configuration interface {
	// Get the current region of this service
	GetRegion() types.DynamicStringValue
	// Is the service running locally?
	GetLocal() types.DynamicBoolValue
	// Get the current service-wide log level
	GetLogLevel() types.DynamicStringValue
	// for interface-compat
	Namespace(string) config.Configuration
	// Get a configuration object configured for the namespace of the current service
	ServiceNamespace() config.Configuration
	// Retrieve an int value from the key/value store. This basically allows 'raw' access to the kv store.
	// You probably want to use the 'prefixed' counterpart to take the namespace of the service into account and
	// uses the proper relative paths of the services.
	GetInt(key string, def int) types.DynamicIntValue
	// Retrieve an int value from the k/v store or an error if it can't be found.
	GetMandatoryInt(key string) (types.DynamicIntValue, error)
	// Retrieve a string value from the key/value store. This basically allows 'raw' access to the kv store.
	// You probably want to use the 'prefixed' counterpart to take the namespace of the service into account and
	// uses the proper relative paths of the services.
	GetString(key string, def string) types.DynamicStringValue
	// Retrieve a string value from the k/v store or an error if it can't be found.
	GetMandatoryString(key string) (types.DynamicStringValue, error)
	// Retrieve a boolean value from the key/value store. This basically allows 'raw' access to the kv store.
	// You probably want to use the 'prefixed' counterpart to take the namespace of the service into account and
	// uses the proper relative paths of the services.
	GetBool(key string, def bool) types.DynamicBoolValue
	// Retrieve a bool value from the k/v store or an error if it can't be found.
	GetMandatoryBool(key string) (types.DynamicBoolValue, error)
	// Retrieve an int value from the key/value store by using the current service name as a prefix, or namespace
	GetPrefixedInt(key string, def int) types.DynamicIntValue
	// Retrieve a string value from the key/value store by using the current service name as a prefix, or namespace
	GetPrefixedString(key string, def string) types.DynamicStringValue
	// Retrieve a boolean value from the key/value store by using the current service name as a prefix, or namespace
	GetPrefixedBool(key string, def bool) types.DynamicBoolValue
	// Get the external base uri for this service (e.g. https://xenon.innius.com/api/foo-service)
	GetExternalServiceBaseUri() string
	// Get the external base uri for this stack (e.g. https://xenon.innius.com)
	GetExternalBaseUri() string
	// Get the local ip of the machine this process is running on
	GetMyIp() (string, error)
	// Register a callback when a value changes within the current scope.
	OnChange(types.Callback)
	// List subscription of dynamic values of this config object
	Subscriptions() []string
}
