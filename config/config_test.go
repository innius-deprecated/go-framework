package config

import (
	"bitbucket.org/to-increase/go-config"
	"bitbucket.org/to-increase/go-framework/types"
)

// As a convenience, make sure we implement the go-config interface
var _ types.Configuration = (*frameworkConfig)(nil)
var _ config.Configuration = (*frameworkConfig)(nil)
