package config

import (
	"bitbucket.org/to-increase/go-config"
	"bitbucket.org/to-increase/go-config/types"
	fwtypes "bitbucket.org/to-increase/go-framework/types"
	"bitbucket.org/to-increase/go-framework/util"
	"bytes"
	"fmt"
	"net/http"
	"time"
"math/rand"
"math"
	"os"
)

type frameworkConfig struct {
	name   string
	my_ip  string
	config config.Configuration
	defaultcb types.Callback
}

const default_stack_name = "no-stack-name-defined"

func NewFrameworkConfig(name string, c config.Configuration) fwtypes.Configuration {
	return NewFrameworkConfigWithDefaultCallback(name, defaultOnChange(), c)
}

func NewFrameworkConfigWithDefaultCallback(name string, cb types.Callback, c config.Configuration) fwtypes.Configuration {
	return &frameworkConfig{
		name:   name,
		my_ip:  "",
		config: c,
		defaultcb: cb,
	}
}

func (c *frameworkConfig) Namespace(ns string) config.Configuration {
	return c.config.Namespace(ns)
}

func (c *frameworkConfig) ServiceNamespace() config.Configuration {
	return c.config.Namespace(c.name)
}

func (c *frameworkConfig) prefix(key string) string {
	return fmt.Sprintf("/%s/%s", c.name, key)
}

func (c *frameworkConfig) GetRegion() types.DynamicStringValue {
	return c.config.GetString("/region", "us-east-1")
}

func (c *frameworkConfig) GetLocal() types.DynamicBoolValue {
	return c.config.GetBool("/local", false)
}

func (c *frameworkConfig) GetLogLevel() types.DynamicStringValue {
	return c.config.GetString(c.prefix("loglevel"), "info")
}

func (c *frameworkConfig) GetInt(key string, def int) types.DynamicIntValue {
	i := c.config.GetInt(key, def)
	i.SetDefaultOnChange(c.defaultcb)
	return i
}

func (c *frameworkConfig) GetMandatoryInt(key string) (types.DynamicIntValue, error) {
	i, err := c.config.GetMandatoryInt(key)
	if err != nil {
		return i, err
	}
	i.SetDefaultOnChange(c.defaultcb)
	return i, err
}

func (c *frameworkConfig) GetString(key string, def string) types.DynamicStringValue {
	s := c.config.GetString(key, def)
	s.SetDefaultOnChange(c.defaultcb)
	return s
}

func (c *frameworkConfig) GetMandatoryString(key string) (types.DynamicStringValue, error) {
	s, err := c.config.GetMandatoryString(key)
	if err != nil {
		return s, err
	}
	s.SetDefaultOnChange(c.defaultcb)
	return s, err
}

func (c *frameworkConfig) GetBool(key string, def bool) types.DynamicBoolValue {
	b := c.config.GetBool(key, def)
	b.SetDefaultOnChange(c.defaultcb)
	return b
}

func (c *frameworkConfig) GetMandatoryBool(key string) (types.DynamicBoolValue, error) {
	b, err := c.config.GetMandatoryBool(key)
	if err != nil {
		return b, err
	}
	b.SetDefaultOnChange(c.defaultcb)
	return b, err
}

func (c *frameworkConfig) GetPrefixedInt(key string, def int) types.DynamicIntValue {
	return c.GetInt(c.prefix(key), def)
}

func (c *frameworkConfig) GetPrefixedString(key string, def string) types.DynamicStringValue {
	return c.GetString(c.prefix(key), def)
}

func (c *frameworkConfig) GetPrefixedBool(key string, def bool) types.DynamicBoolValue {
	return c.GetBool(c.prefix(key), def)
}

func (c *frameworkConfig) GetExternalServiceBaseUri() string {
	s := c.config.GetString("/stack", default_stack_name).Value()
	return util.GetExternalBaseServiceUri(s, c.name)
}

func (c *frameworkConfig) GetExternalBaseUri() string {
	s := c.config.GetString("/stack", default_stack_name).Value()
	return util.GetExternalBaseUri(s)
}

func (c *frameworkConfig) GetMyIp() (string, error) {
	if c.GetLocal().Value() {
		return "127.0.0.1", nil
	}
	if c.my_ip == "" {
		client := &http.Client{
			Timeout: 1 * time.Second,
		}
		resp, err := client.Get("http://169.254.169.254/latest/meta-data/local-ipv4")
		if err != nil {
			return "", err
		}
		defer resp.Body.Close()
		buf := new(bytes.Buffer)
		buf.ReadFrom(resp.Body)
		c.my_ip = buf.String()
	}

	return c.my_ip, nil
}

func (c *frameworkConfig) OnChange(cb types.Callback) {
	c.config.OnChange(cb)
}

func (c *frameworkConfig) Subscriptions() []string {
	return c.config.Subscriptions()
}

func defaultOnChange() types.Callback {
	return func(path types.Path, value interface{}) {
		i := math.Mod(rand.Float64()*1000, 30) //Small random restart time so that there will always be an instance available for new requests (assuming there is more than 1)
		deadClock := time.NewTimer(time.Duration(int64(math.Floor(i)))*time.Second)
		<- deadClock.C
		os.Exit(42) //Unsure what would be a good exit code
	}
}
